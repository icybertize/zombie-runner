package th.ac.kmitl.it.mdp2014.zombierunner.screens;

import java.net.InetAddress;
import java.util.List;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.generators.FontGenerator;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.server.RequestServerInfoPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.ButtonUI;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.HostInfo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class ManualJoinScreen extends ScreenAdapter {
	
	private static final String TAG = "ManaulJoinScreen";
	
	private ZombieRunner game;
	
	private WorldManager worldManager;
	private NetworkManager networkManager;
	
	public Label leLabel;
	public HostInfo hostInfo;
	private List<InetAddress> hosts;
	private TextButton[] hostButtons;
	private ChangeListener[] changeListerners;
    
    private ButtonUI buttons;
	
	public ManualJoinScreen(ZombieRunner game) {
		this.game = game;
		
		worldManager = game.getWorldManager();
		worldManager.setManualJoinScreen(this);
		networkManager = game.getNetworkManager();
		
		createUI();
		
		createManualJoinUI();
        
        blahhh();
	}
	
	private void createManualJoinUI() {
	    Stage stage = buttons.getStage();
        Skin skin = buttons.getSkin();
        
        int buttonFontSize = Gdx.graphics.getHeight() / 10;
        BitmapFont font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = font;
        skin.add("default", labelStyle);
        
        Label label = new Label("Select Server", skin);
        label.setPosition(50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 500 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        stage.addActor(label);
        
        buttonFontSize = Gdx.graphics.getHeight() / 14;
        font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        labelStyle = new LabelStyle();
        labelStyle.font = font;
        skin.add("default", labelStyle);
        
        leLabel = new Label("Server Info: ", skin);
        leLabel.setPosition(50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 425 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        stage.addActor(leLabel);
        
        hostButtons = new TextButton[5];
        changeListerners = new ChangeListener[5];
        
        buttonFontSize = Gdx.graphics.getHeight() / 12;
        font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        TextButtonStyle style = new TextButtonStyle();
        style.font = font;
        style.up = skin.newDrawable("white", Color.BLACK);
        style.down = skin.newDrawable("white", Color.RED);
        skin.add("default", style);
        
        for (int i = 0; i < 5; i++) {
            hostButtons[i] = new TextButton(" 192.168.0.1 ", skin);
            hostButtons[i].setPosition(50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, (325 - (75*i)) * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
            hostButtons[i].setVisible(false);
            changeListerners[i] = new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    
                }
            };
            hostButtons[i].addListener(changeListerners[i]);
            stage.addActor(hostButtons[i]);
        }
    }

    @Override
	public void show() {
		Gdx.app.log(TAG, "show()");
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		buttons.render();
	}
    
    @Override
    public void dispose() {
        buttons.dispose();
    }

    private void createUI() {
        buttons = new ButtonUI();
        
        buttons.addButton("Refresh", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
                blahhh();
            }
        });
        
        buttons.addButton("OK", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
                try {
                    System.out.println("MAINFUCKSHIT");
                    networkManager.manualJoin(hostInfo.getAddress());
                    game.setScreen(new LobbyScreen(game));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        
        buttons.addButton("Cancel", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
                game.setScreen(new MainMenuScreen(game));
            }
        });
        
        buttons.create();
    }

    private void blahhh() {
        hosts = networkManager.getHosts();
        System.out.println("HOSTS " + hosts.size());
        for (int i = 0; i < Math.min(5, hosts.size()); i++) {
            final int fI = i;
            hostButtons[i].setText(" " + hosts.get(i) + " ");
            hostButtons[i].setVisible(true);
            hostButtons[i].removeListener(changeListerners[i]);
            changeListerners[i] = new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    try {
                        networkManager.manualJoin(hosts.get(fI));
                        RequestServerInfoPacket packet = new RequestServerInfoPacket();
                        networkManager.send(packet);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            hostButtons[i].addListener(changeListerners[i]);
        }
    }

}
