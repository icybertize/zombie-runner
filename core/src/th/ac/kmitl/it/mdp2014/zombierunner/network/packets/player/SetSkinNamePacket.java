package th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player;

import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.Packet;

public class SetSkinNamePacket extends Packet {
	
	public static enum SkinName { HUMAN, BATMAN, ZOMBIE, ULTRON };
	
	private SkinName skinName;

	public SkinName getSkinName() {
		return skinName;
	}

	public void setSkinName(SkinName skinName) {
		this.skinName = skinName;
	}
	
	public static String getActualSkinName(SkinName skinName) {
		switch (skinName) {
		
		case HUMAN:
			return "human";
			
		case BATMAN:
			return "batman";
			
		case ZOMBIE:
			return "zombie";
			
		case ULTRON:
			return "ultron";
			
		default:
			return "human";
		}
	}
	
}
