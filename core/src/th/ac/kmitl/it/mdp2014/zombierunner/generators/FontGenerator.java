package th.ac.kmitl.it.mdp2014.zombierunner.generators;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public abstract class FontGenerator {
	
	public static BitmapFont generateFont(String path, int size) {
		BitmapFont font = null;
		
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(path));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		
		parameter.size = size;
		
		font = generator.generateFont(parameter);
		
		return font;
	}

}
