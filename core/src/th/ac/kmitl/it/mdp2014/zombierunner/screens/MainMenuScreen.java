package th.ac.kmitl.it.mdp2014.zombierunner.screens;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.ButtonUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class MainMenuScreen extends ScreenAdapter {
	
	private static final String TAG = "MainMenuScreen";
	
	private ZombieRunner game;
	
	private SpriteBatch batch;
	private Texture background;
	
	private ButtonUI ui;
	
	public MainMenuScreen(ZombieRunner game) {
		this.game = game;
		
		batch = new SpriteBatch();
		background = new Texture("backgrounds/mainMenu.png");
		
		createUI();
	}
	
	@Override
	public void show() {
		Gdx.app.log(TAG, "show()");
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.13f, 0.13f, 0.13f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		batch.draw(background, 0, 0, (background.getWidth() * Gdx.graphics.getHeight()) / background.getHeight(), Gdx.graphics.getHeight());
		batch.end();
		
		ui.render();
	}
	
	@Override
	public void dispose() {
		ui.dispose();
	}
	
	private void createUI() {
		ui = new ButtonUI();
		
		ui.addButton("Create Game", new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
				game.setScreen(new CreateGameScreen(game));
			}
		});
		
		ui.addButton("Quick Join", new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
				game.setScreen(new QuickJoinScreen(game));
			}
		});
		
		ui.addButton("Manual Join", new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
				game.setScreen(new ManualJoinScreen(game));
			}
		});
		
		ui.addButton("Settings", new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
				game.setScreen(new SettingsScreen(game));
			}
		});
		
		ui.create();
	}

}
