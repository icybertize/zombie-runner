package th.ac.kmitl.it.mdp2014.zombierunner.managers;

import java.net.InetAddress;
import java.util.List;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Player;
import th.ac.kmitl.it.mdp2014.zombierunner.listeners.ClientListener;
import th.ac.kmitl.it.mdp2014.zombierunner.listeners.ServerListener;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.HelloPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.Packet;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetPositionPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetReadyPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetSkinNamePacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetTeamPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.DoSkill1Packet;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.DoSkill2Packet;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.StopWalkingPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkLeftPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkRightPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkUpPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.server.RequestServerInfoPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.server.ServerInfoPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.CreatePlayerPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.RemovePlayerPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.RequestOtherPlayersPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.SetMapPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.Map;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.EndPoint;
import com.esotericsoftware.kryonet.Server;

public class NetworkManager {

	private static final String TAG = "NetworkManager";

	private static final int TCP_PORT = 55555;
	private static final int UDP_PORT = 55556;
	private static final int TIMEOUT = 1000;

	private ZombieRunner game;

	private Server server;
	private Client client;

	public NetworkManager(ZombieRunner game) {
		this.game = game;
	}

	public void startServer(String name) throws Exception {
		server = new Server();
		server.start();
		server.bind(TCP_PORT, UDP_PORT);
		server.addListener(new ServerListener(game));

		registerClasses(server);
	}

	public void startClient() {
		client = new Client();
		client.start();
		client.addListener(new ClientListener(game));

		registerClasses(client);
	}

	public void quickJoin() throws Exception {
		if (!isServer()) {
			InetAddress host = client.discoverHost(UDP_PORT, TIMEOUT);
			manualJoin(host);
		}
	}

	
	public List<InetAddress> getHosts() {
	    return client.discoverHosts(UDP_PORT, TIMEOUT);
	}
	

	public void manualJoin(InetAddress host) throws Exception {
		client.connect(TIMEOUT, host, TCP_PORT, UDP_PORT);
	}

	public void disconnect() {
		client.close();
	}

	public void closeServer() {
		server.close();
		server = null;
	}

	public boolean isServer() {
		return server != null;
	}

	public void send(Packet packet) {
		if (isServer()) {
			sendToClients(packet);
		} else {
			sendToServer(packet);
		}
	}

	private void sendToServer(Packet packet) {
		client.sendUDP(packet);

		Gdx.app.log(TAG, "sendToServer() -> "
				+ packet.getClass().getSimpleName());
	}

	private void sendToClients(Packet packet) {
		server.sendToAllUDP(packet);

		Gdx.app.log(TAG, "sendToClients() -> "
				+ packet.getClass().getSimpleName());
	}

	public void sendToOtherClients(int connectionID, Object object) {
		for (Connection connection : server.getConnections()) {
			if (connection.getID() != connectionID)
				connection.sendUDP(object);
		}

		Gdx.app.log(TAG, "sendToOtherClients() -> "
				+ object.getClass().getSimpleName());
	}

	private void registerClasses(EndPoint endPoint) {
		Kryo kryo = endPoint.getKryo();

		kryo.register(HelloPacket.class);

		kryo.register(SetTeamPacket.class);
		kryo.register(SetTeamPacket.Team.class);
		kryo.register(SetSkinNamePacket.class);
		kryo.register(SetSkinNamePacket.SkinName.class);
		kryo.register(SetReadyPacket.class);
		kryo.register(SetPositionPacket.class);
		kryo.register(Player.Direction.class);

		kryo.register(CreatePlayerPacket.class);
		kryo.register(RemovePlayerPacket.class);
		kryo.register(RequestOtherPlayersPacket.class);
		kryo.register(SetMapPacket.class);
		kryo.register(Map.class);

		kryo.register(WalkUpPacket.class);
		kryo.register(WalkLeftPacket.class);
		kryo.register(WalkRightPacket.class);
		kryo.register(StopWalkingPacket.class);
        kryo.register(DoSkill1Packet.class);
        kryo.register(DoSkill2Packet.class);
		
		kryo.register(RequestServerInfoPacket.class);
        kryo.register(ServerInfoPacket.class);
	}
	
    public Client getClient() {
        return client;
    }

}
