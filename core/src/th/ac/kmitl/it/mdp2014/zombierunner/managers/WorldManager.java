package th.ac.kmitl.it.mdp2014.zombierunner.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Player;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Syringe;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetTeamPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.screens.LobbyScreen;
import th.ac.kmitl.it.mdp2014.zombierunner.screens.ManualJoinScreen;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.Map;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

public class WorldManager {
    
    public static final float PPM = 100;
    public static final short PLAYER_BITS = 1;
    public static final short GROUND_BITS = 2;
    public static final short WALL_BITS = 4;
    public static final short LADDER_BITS = 8;
    public static final short SYRINGE_BITS = 16;
    public static final short HUMAN_WALL_BITS = 32;
    public static final short SYRINGE_RADIUS_BITS = 64;
    public static final short INFECTION_BITS = 128;
    
    private static final float TIME_STEP = 1 / 60f;
    
    @SuppressWarnings("unused")
    private ZombieRunner game;
    
    private World world;
    private Map map;
    
    private Player player;
    private HashMap<Integer, Player> otherPlayers;
    
    private List<Syringe> syringes;
    
    private LobbyScreen lobbyScreen;
    private ManualJoinScreen manualJoinScreen;
    
    private float a;
    
    public int cameraPlayerIndex = 0;
    
    public WorldManager(ZombieRunner game) {
        this.game = game;
        
        otherPlayers = new HashMap<Integer, Player>();
        
        syringes = new ArrayList<Syringe>();
    }
    
    public void createWorld() {
        world = new World(new Vector2(0, -9.81f), true);
        otherPlayers = new HashMap<Integer, Player>();
        syringes = new ArrayList<Syringe>();
        cameraPlayerIndex = 0;
    }
    
    public void updateWorld(float delta) {
        float frameTime = Math.min(delta, 0.25f);
        a += frameTime;
        while (a >= TIME_STEP) {
            world.step(TIME_STEP, 6, 2);
            a -= TIME_STEP;
        }
    }
    
    public void createPlayer(Player player) {
        Body body = world.createBody(player.getBodyDef());
        player.setBody(body);
        body.createFixture(player.getFixtureDef()).setUserData(player);
        body.createFixture(player.getInfectionFixtureDef()).setUserData(player);
    }
    
    public void createOtherPlayer(int connectionId, Player player) {
        createPlayer(player);
        otherPlayers.put(connectionId, player);
    }
    
    public void removePlayer(int connectionId) {
        Player otherPlayer = otherPlayers.get(connectionId);
        if (!world.isLocked()) world.destroyBody(otherPlayer.getBody());
        otherPlayers.remove(connectionId);
    }
    
    public World getWorld() {
        return world;
    }
    
    public Map getMap() {
        return map;
    }
    
    public void setMap(Map map) {
        this.map = map;
    }
    
    public Player getPlayer() {
        return player;
    }
    
    public void setPlayer(Player player) {
        this.player = player;
    }
    
    public HashMap<Integer, Player> getOtherPlayers() {
        return otherPlayers;
    }
    
    public List<Syringe> getSyringes() {
        return syringes;
    }
    
    public int getCollectedSyringes() {
        int collectedSyringes = 0;
        for (Syringe syringe : syringes)
            if (syringe.isPickedUp()) collectedSyringes++;
        return collectedSyringes;
    }
    
    public int getAliveHumanCount() {
        int humanCount = player.getTeam() == SetTeamPacket.Team.HUMAN && player.getHP() > 0 ? 1 : 0;
        for (Player player : otherPlayers.values())
            if (player.getTeam() == SetTeamPacket.Team.HUMAN && player.getHP() > 0) humanCount++;
        return humanCount;
    }
    
    public int getAliveZombieCount() {
        int zombieCount = player.getTeam() == SetTeamPacket.Team.ZOMBIE && player.getHP() > 0 ? 1 : 0;
        for (Player player : otherPlayers.values())
            if (player.getTeam() == SetTeamPacket.Team.ZOMBIE && player.getHP() > 0) zombieCount++;
        return zombieCount;
    }
    
    public int getHumanCount() {
        int humanCount = player.getTeam() == SetTeamPacket.Team.HUMAN ? 1 : 0;
        for (Player player : otherPlayers.values())
            if (player.getTeam() == SetTeamPacket.Team.HUMAN) humanCount++;
        return humanCount;
    }
    
    public int getZombieCount() {
        int zombieCount = player.getTeam() == SetTeamPacket.Team.ZOMBIE ? 1 : 0;
        for (Player player : otherPlayers.values())
            if (player.getTeam() == SetTeamPacket.Team.ZOMBIE) zombieCount++;
        return zombieCount;
    }
    
    public List<Player> getHumanPlayers() {
        List<Player> players = new ArrayList<Player>();
        for (Player player : otherPlayers.values())
            if (player.getTeam() == SetTeamPacket.Team.HUMAN) players.add(player);
        return players;
    }
    
    public List<Player> getZombiePlayers() {
        List<Player> players = new ArrayList<Player>();
        for (Player player : otherPlayers.values())
            if (player.getTeam() == SetTeamPacket.Team.ZOMBIE) players.add(player);
        return players;
    }

    
    public LobbyScreen getLobbyScreen() {
        return lobbyScreen;
    }

    
    public void setLobbyScreen(LobbyScreen lobbyScreen) {
        this.lobbyScreen = lobbyScreen;
    }

    public ManualJoinScreen getManualJoinScreen() {
        return manualJoinScreen;
    }
    
    public void setManualJoinScreen(ManualJoinScreen manualJoinScreen) {
        this.manualJoinScreen = manualJoinScreen;
    }
    
}
