package th.ac.kmitl.it.mdp2014.zombierunner.screens;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Player;
import th.ac.kmitl.it.mdp2014.zombierunner.generators.FontGenerator;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.ButtonUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class EndGameScreen extends ScreenAdapter {
	
	private static final String TAG = "EndGameScreen";
	
	private ZombieRunner game;
	
	private WorldManager worldManager;
	private NetworkManager networkManager;
	
	private ButtonUI buttons;
	
	public EndGameScreen(ZombieRunner game) {
		this.game = game;
		
		worldManager = game.getWorldManager();
		networkManager = game.getNetworkManager();
		
		createUI();
		
		createEndGameUI();
	}
	
	@Override
	public void show() {
		Gdx.app.log(TAG, "show()");
	}
	
	@Override
	public void render(float delta) {
        Gdx.gl.glClearColor(0.13f, 0.13f, 0.13f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		buttons.render();
	}
	
	private void createEndGameUI() {
	    Stage stage = buttons.getStage();
        Skin skin = buttons.getSkin();
        
        int buttonFontSize = Gdx.graphics.getHeight() / 6;
        BitmapFont font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Bold.ttf", buttonFontSize);
        
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = font;
        skin.add("default", labelStyle);
        
        String result = "LOL";
        if (worldManager.getCollectedSyringes() == worldManager.getSyringes().size()) {
            result = "Human Win!";
        } else {
            result = "Zombie Win!";
        }
        
        Table table = new Table();
        table = new Table();
        table.setFillParent(true);
        stage.addActor(table);
        
        Label resultLabel = new Label(result, skin);
        resultLabel.setPosition(buttonFontSize / 2, Gdx.graphics.getHeight() - resultLabel.getHeight() - buttonFontSize / 2);
        table.add(resultLabel);
        table.row().spaceBottom(10);
        
        buttonFontSize = Gdx.graphics.getHeight() / 10;
        font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        labelStyle = new LabelStyle();
        labelStyle.font = font;
        skin.add("default", labelStyle);
        
        Label humanLabel = new Label("Remaining Human: " + worldManager.getAliveHumanCount() + "/" + worldManager.getHumanCount(), skin);
        table.add(humanLabel);
        table.row();
        
        Label zombieLabel = new Label("Remaining Zombie: " + worldManager.getAliveZombieCount() + "/" + worldManager.getZombieCount(), skin);
        table.add(zombieLabel);
        table.row();
        
        Label syringesLabel = new Label("Collected Syringes: " + worldManager.getCollectedSyringes() + "/" + worldManager.getSyringes().size(), skin);
        table.add(syringesLabel);
        table.row();
	}
	
	private void createUI() {
        buttons = new ButtonUI();
        
        buttons.addButton("OK", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (networkManager.isServer()) {
                    networkManager.closeServer();
                } else {
                    networkManager.disconnect();
                }
                game.setWorldManager(new WorldManager(game));
                game.setNetworkManager(new NetworkManager(game));
                game.getWorldManager().createWorld();
                game.getWorldManager().setPlayer(new Player(Gdx.app.getPreferences(ZombieRunner.PREFERENCES_FILE_NAME).getString("playerName")));
                game.getNetworkManager().startClient();
                game.setScreen(new MainMenuScreen(game));
            }
        });
        
        buttons.create();
    }

}
