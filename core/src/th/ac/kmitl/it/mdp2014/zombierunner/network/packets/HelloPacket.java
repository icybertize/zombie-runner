package th.ac.kmitl.it.mdp2014.zombierunner.network.packets;

public class HelloPacket extends Packet {
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
