package th.ac.kmitl.it.mdp2014.zombierunner.screens;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.generators.FontGenerator;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetSkinNamePacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetSkinNamePacket.SkinName;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.ButtonUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class SettingsScreen extends ScreenAdapter {
    
    private static final String TAG = "SettingsScreen";
    
    private ZombieRunner game;
    
    private ButtonUI buttons;
    
    private Preferences preferences;
    
    private String playerName;
    private int humanSkin;
    private int zombieSkin;
    private boolean isPlaySound;
    
    private SpriteBatch batch;
    
    private Texture humanSkinTexture;
    private Texture zombieSkinTexture;
    
    public SettingsScreen(ZombieRunner game) {
        this.game = game;
        
        createButtonUI();
        
        preferences = Gdx.app.getPreferences(ZombieRunner.PREFERENCES_FILE_NAME);
        
        playerName = preferences.getString("playerName", "Steve");
        humanSkin = preferences.getInteger("humanSkin", 0);
        zombieSkin = preferences.getInteger("zombieSkin", 2);
        isPlaySound = preferences.getBoolean("isPlaySound", true);
        
        batch = new SpriteBatch();
        
        humanSkinTexture = new Texture("characters/" + SetSkinNamePacket.getActualSkinName(SkinName.values()[humanSkin]) + "/face.png");
        
        zombieSkinTexture = new Texture("characters/" + SetSkinNamePacket.getActualSkinName(SkinName.values()[zombieSkin]) + "/face.png");
        
        createPlayerUI();
    }
    
    @Override
    public void show() {
        Gdx.app.log(TAG, "show()");
    }
    
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.13f, 0.13f, 0.13f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        buttons.render();
        
        batch.begin();
        batch.draw(humanSkinTexture, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, humanSkinTexture.getWidth() * 3 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, humanSkinTexture.getHeight() * 3 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        batch.draw(zombieSkinTexture, 500 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, humanSkinTexture.getWidth() * 3 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, humanSkinTexture.getHeight() * 3 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        batch.end();
    }
    
    @Override
    public void dispose() {
        buttons.dispose();
    }
    
    private void createPlayerUI() {
        Stage stage = buttons.getStage();
        Skin skin = buttons.getSkin();
        
        int buttonFontSize = Gdx.graphics.getHeight() / 8;
        BitmapFont font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = font;
        skin.add("default", labelStyle);
        
        TextFieldStyle textFieldStyle = new TextFieldStyle();
        textFieldStyle.cursor = skin.newDrawable("white", Color.RED);
        textFieldStyle.font = font;
        textFieldStyle.fontColor = Color.RED;
        skin.add("default", textFieldStyle);
        
        Label label = new Label("Player Name:", skin);
        label.setPosition(buttonFontSize / 2, Gdx.graphics.getHeight() - label.getHeight() - buttonFontSize / 2);
        stage.addActor(label);
        
        TextField textField = new TextField(playerName, skin);
        textField.setWidth(Gdx.graphics.getWidth() - label.getWidth() - buttonFontSize);
        textField.setPosition(buttonFontSize / 2 + label.getWidth() + buttonFontSize / 2, Gdx.graphics.getHeight() - label.getHeight() - buttonFontSize / 2);
        textField.setMaxLength(8);
        textField.setTextFieldListener(new TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                playerName = textField.getText();
            }
        });
        stage.addActor(textField);
        
        ImageButton humanLeftButton = new ImageButton(new Image(new Texture("buttons/arrowLeft.png")).getDrawable());
        humanLeftButton.setPosition(75 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        humanLeftButton.setSize(100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        humanLeftButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                humanSkin--;
                if (humanSkin < 0) humanSkin = SkinName.values().length / 2 - 1;
                humanSkinTexture = new Texture("characters/" + SetSkinNamePacket.getActualSkinName(SkinName.values()[humanSkin]) + "/face.png");
            }
        });
        stage.addActor(humanLeftButton);
        
        ImageButton humanRightButton = new ImageButton(new Image(new Texture("buttons/arrowRight.png")).getDrawable());
        humanRightButton.setPosition(325 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        humanRightButton.setSize(100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        humanRightButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                humanSkin++;
                if (humanSkin > SkinName.values().length / 2 - 1) humanSkin = 0;
                humanSkinTexture = new Texture("characters/" + SetSkinNamePacket.getActualSkinName(SkinName.values()[humanSkin]) + "/face.png");
            }
        });
        stage.addActor(humanRightButton);
        
        ImageButton zombieLeftButton = new ImageButton(new Image(new Texture("buttons/arrowLeft.png")).getDrawable());
        zombieLeftButton.setPosition(475 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        zombieLeftButton.setSize(100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        zombieLeftButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                zombieSkin--;
                if (zombieSkin < SkinName.values().length / 2) zombieSkin = SkinName.values().length - 1;
                zombieSkinTexture = new Texture("characters/" + SetSkinNamePacket.getActualSkinName(SkinName.values()[zombieSkin]) + "/face.png");
            }
        });
        stage.addActor(zombieLeftButton);
        
        ImageButton zombieRightButton = new ImageButton(new Image(new Texture("buttons/arrowRight.png")).getDrawable());
        zombieRightButton.setPosition(725 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        zombieRightButton.setSize(100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        zombieRightButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                zombieSkin++;
                if (zombieSkin > SkinName.values().length - 1) zombieSkin = SkinName.values().length / 2;
                zombieSkinTexture = new Texture("characters/" + SetSkinNamePacket.getActualSkinName(SkinName.values()[zombieSkin]) + "/face.png");
            }
        });
        stage.addActor(zombieRightButton);
        
        buttonFontSize = Gdx.graphics.getHeight() / 10;
        font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        Pixmap pixmap = new Pixmap(25, 25, Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("white_", new Texture(pixmap));
        
        CheckBoxStyle checkBoxStyle = new CheckBoxStyle();
        checkBoxStyle.checkboxOff = skin.newDrawable("white_", Color.BLACK);
        checkBoxStyle.checkboxOn = skin.newDrawable("white_", Color.RED);
        checkBoxStyle.font = font;
        skin.add("default", checkBoxStyle);
        
        final CheckBox isPlaySoundCheckBox = new CheckBox(" Sound?", skin);
        isPlaySoundCheckBox.setPosition(label.getWidth() / 2 + textField.getWidth() + 20, Gdx.graphics.getHeight() - label.getHeight() - buttonFontSize / 2);
        isPlaySoundCheckBox.setChecked(isPlaySound);
        isPlaySoundCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                isPlaySound = isPlaySoundCheckBox.isChecked();
                game.getSounds().setPlaySound(isPlaySound);
                if (isPlaySound) game.getSounds().loop(game.getSounds().menuMusic);
                else game.getSounds().menuMusic.stop();
            }
        });
        stage.addActor(isPlaySoundCheckBox);
    }
    
    private void createButtonUI() {
        buttons = new ButtonUI();
        
        buttons.addButton("OK", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
                preferences.putString("playerName", playerName);
                preferences.putInteger("humanSkin", humanSkin);
                preferences.putInteger("zombieSkin", zombieSkin);
                preferences.putBoolean("isPlaySound", isPlaySound);
                preferences.flush();
                game.initialize(playerName);
            }
        });
        
        buttons.create();
    }
    
}
