package th.ac.kmitl.it.mdp2014.zombierunner.entities;

import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.PLAYER_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.PPM;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.SYRINGE_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.SYRINGE_RADIUS_BITS;
import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Syringe extends Sprite {
    
    public static final int SPRITE_WIDTH = 50;
    public static final int SPRITE_HEIGHT = 50;
    
    private float boxWidth = 25;
    private float boxHeight = 25;
    
    private BodyDef bodyDef;
    private FixtureDef fixtureDef;
    private FixtureDef radiusFixtureDef;
    
    private boolean isPickedUp;
    
    private Texture texture;
    private Texture radiusTexture;

	public Syringe(String name) {
		super(name);
		
		bodyDef = new BodyDef();
        bodyDef.type = BodyType.StaticBody;
        bodyDef.position.set(ZombieRunner.VIEWPORT_WIDTH / 2, ZombieRunner.VIEWPORT_HEIGHT);
        
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(boxWidth / PPM, boxHeight / PPM);

        fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = SYRINGE_BITS;
        fixtureDef.filter.maskBits = PLAYER_BITS;
        fixtureDef.isSensor = true;
        
        CircleShape radiusShape = new CircleShape();
        radiusShape.setRadius(boxWidth * 5 / PPM);
        
        radiusFixtureDef = new FixtureDef();
        radiusFixtureDef.shape = radiusShape;
        radiusFixtureDef.filter.categoryBits = SYRINGE_RADIUS_BITS;
        radiusFixtureDef.filter.maskBits = PLAYER_BITS;
        radiusFixtureDef.isSensor = true;
		
		isPickedUp = false;
		
		texture = null;
		radiusTexture = null;
	}
	
	public Syringe(String name, float x, float y) {
	    this(name);
	    bodyDef.position.set(x, y);
	}
	
	public void render(Batch batch) {
	    batch.begin();
	    batch.draw(texture, body.getPosition().x * PPM - SPRITE_WIDTH / 2, body.getPosition().y * PPM - SPRITE_HEIGHT / 2);
        batch.draw(radiusTexture, body.getPosition().x * PPM - 250 / 2, body.getPosition().y * PPM - 250 / 2, 250, 250);
	    batch.end();
	}
	
	public void createTexture() {
	    texture = new Texture("hud/syringe.png");
	    radiusTexture = new Texture("hud/syringeRadius.png");
	}
	
	public boolean isCreatedTexture() {
	    return texture != null && radiusTexture != null;
	}
    
    public BodyDef getBodyDef() {
        return bodyDef;
    }
    
    public FixtureDef getFixtureDef() {
        return fixtureDef;
    }

    
    public FixtureDef getRadiusFixtureDef() {
        return radiusFixtureDef;
    }

    public boolean isPickedUp() {
        return isPickedUp;
    }
    
    public void setPickedUp(boolean isPickedUp) {
        this.isPickedUp = isPickedUp;
    }

}
