package th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player;

import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.Packet;

public class SetTeamPacket extends Packet {
	
	public static enum Team { HUMAN, ZOMBIE };
	
	private Team team;

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

}
