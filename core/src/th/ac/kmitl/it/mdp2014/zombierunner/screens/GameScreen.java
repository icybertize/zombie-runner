package th.ac.kmitl.it.mdp2014.zombierunner.screens;

import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.GROUND_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.HUMAN_WALL_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.LADDER_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.PLAYER_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.PPM;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.WALL_BITS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Player;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Syringe;
import th.ac.kmitl.it.mdp2014.zombierunner.generators.FontGenerator;
import th.ac.kmitl.it.mdp2014.zombierunner.inputprocessors.PlayerInputProcessor;
import th.ac.kmitl.it.mdp2014.zombierunner.listeners.WorldContactListener;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetPositionPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetTeamPacket.Team;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.HUD;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class GameScreen extends ScreenAdapter {
    
    private static final String TAG = "TestScreen";
    
    @SuppressWarnings("unused")
    private ZombieRunner game;
    
    private NetworkManager networkManager;
    
    private WorldManager worldManager;
    private World world;
    
    private Player player;
    private HashMap<Integer, Player> otherPlayers;
    private List<Syringe> syringes;
    
    private OrthographicCamera box2DCamera;
    private OrthographicCamera camera;
    
    private HUD hud;
    
    private TiledMap map;
    private OrthogonalTiledMapRenderer mapRenderer;
    
    private Batch batch;
    private Texture wallTexture;
    private Texture bgTexture;
    
    private List<Body> humanWall;
    private List<Body> groundWall;
    private List<Body> replaceWall;
    private List<Body> replaceHumanWall;
    
    private BitmapFont playerNameFont;
    
    private Box2DDebugRenderer debugRenderer;
    
    private Player cameraPlayer;
    
    public GameScreen(ZombieRunner game) {
        this.game = game;
        
        worldManager = game.getWorldManager();
        world = worldManager.getWorld();
        world.setContactListener(new WorldContactListener(game));
        
        networkManager = game.getNetworkManager();
        
        player = worldManager.getPlayer();
        otherPlayers = worldManager.getOtherPlayers();
        syringes = worldManager.getSyringes();
        
        box2DCamera = new OrthographicCamera();
        box2DCamera.setToOrtho(false, ZombieRunner.VIEWPORT_WIDTH, ZombieRunner.VIEWPORT_HEIGHT);
        box2DCamera.update();
        
        camera = new OrthographicCamera();
        camera.setToOrtho(false, ZombieRunner.SCREEN_WIDTH, ZombieRunner.SCREEN_HEIGHT);
        camera.update();
        
        hud = new HUD(game);
        
        map = new TmxMapLoader().load("maps/" + worldManager.getMap() + ".tmx");
        mapRenderer = new OrthogonalTiledMapRenderer(map, 1 / 1f);
        
        batch = new SpriteBatch();
        wallTexture = new Texture("maps/objects/wall.png");
        bgTexture = new Texture("blocks/bgReplace.png");
        humanWall = new ArrayList<Body>();
        groundWall = new ArrayList<Body>();
        replaceWall = new ArrayList<Body>();
        replaceHumanWall = new ArrayList<Body>();
        
        debugRenderer = new Box2DDebugRenderer();
        
        cameraPlayer = player;
        
        playerNameFont = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", 20);
        
        createMap();
        
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(new PlayerInputProcessor(game));
        inputMultiplexer.addProcessor(hud.getStage());
        
        Gdx.input.setInputProcessor(inputMultiplexer);
        
        game.getSounds().menuMusic.stop();
        game.getSounds().loop(game.getSounds().inGameMusic);
    }
    
    @Override
    public void show() {
        Gdx.app.log(TAG, "show()");
    }
    
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.62f, 0.62f, 0.62f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        worldManager.updateWorld(delta);
        
        box2DCamera.position.set(cameraPlayer.getBody().getPosition().x, cameraPlayer.getBody().getPosition().y, 0);
        box2DCamera.update();
        
        camera.position.set(cameraPlayer.getBody().getPosition().x * PPM, cameraPlayer.getBody().getPosition().y * PPM, 0);
        camera.update();
        
        mapRenderer.setView(camera);
        mapRenderer.render();
        
        batch.setProjectionMatrix(camera.combined);
        
        for (Body body : replaceHumanWall) {
            batch.begin();
            batch.draw(bgTexture, (body.getPosition().x * PPM) - 25, (body.getPosition().y * PPM) - 50);
            batch.draw(bgTexture, (body.getPosition().x * PPM) - 25, (body.getPosition().y * PPM));
            batch.end();
        }
        
//        for (Body body : replaceWall) {
//            batch.begin();
//            batch.draw(bgTexture, (body.getPosition().x * PPM) - 25, (body.getPosition().y * PPM) - 25);
//            batch.end();
//        }
        
        for (Body body : humanWall) {
            batch.begin();
            batch.draw(wallTexture, (body.getPosition().x * PPM) - 25, (body.getPosition().y * PPM) - 50);
            batch.end();
        }
        
        if (isSkill1Done(player)) player.doneSkill1();
        
        if (isSkill2Done(player)) player.doneSkill2();
        
        if (isSkill1Ready(player)) {
            player.setSkill1Ready(true);
            hud.setSkill1Ready(true);
        } else {
            player.setSkill1Ready(false);
            hud.setSkill1Ready(false);
        }
        
        if (isSkill2Ready(player)) {
            player.setSkill2Ready(true);
            hud.setSkill2Ready(true);
        } else {
            player.setSkill2Ready(false);
            hud.setSkill2Ready(false);
        }
        
        for (Player otherPlayer : otherPlayers.values()) {
            if (isSkill1Done(otherPlayer)) otherPlayer.doneSkill1();
            
            if (isSkill2Done(otherPlayer)) otherPlayer.doneSkill2();
            
            if (isSkill1Ready(otherPlayer)) {
                otherPlayer.setSkill1Ready(true);
            } else {
                otherPlayer.setSkill1Ready(false);
            }
            
            if (isSkill2Ready(otherPlayer)) {
                otherPlayer.setSkill2Ready(true);
            } else {
                otherPlayer.setSkill2Ready(false);
            }
        }
        
        if (!player.isCreatedAnimations()) player.createAnimations();
        if (player.getHP() > 0) player.render(batch);
        
        if (player.getHP() > 0) playerNameFont.setColor(player.getTeamColor());
        if (player.getHP() > 0) drawPlayerName(player);
        
        if (player.isSkill1Started()) {
            playerNameFont.setColor(Color.ORANGE);
            batch.begin();
            String skillName = "lol";
            if (player.getTeam() == Team.HUMAN) skillName = "Dig Down";
            if (player.getTeam() == Team.ZOMBIE) skillName = "Sprint";
            playerNameFont.draw(batch, skillName, player.getBody().getPosition().x * PPM - player.getName().length() * playerNameFont.getSpaceWidth() - (skillName.length() * playerNameFont.getSpaceWidth()) / 2, player.getBody().getPosition().y * PPM + Player.SPRITE_HEIGHT / 2 + playerNameFont.getLineHeight() + 25);
            batch.end();
        }
        
        if (player.isSkill2Started()) {
            playerNameFont.setColor(Color.ORANGE);
            batch.begin();
            String skillName = "lol";
            if (player.getTeam() == Team.HUMAN) skillName = "Build Wall";
            if (player.getTeam() == Team.ZOMBIE) skillName = "Blast Wall";
            playerNameFont.draw(batch, skillName, player.getBody().getPosition().x * PPM - player.getName().length() * playerNameFont.getSpaceWidth() - (skillName.length() * playerNameFont.getSpaceWidth()) / 2, player.getBody().getPosition().y * PPM + Player.SPRITE_HEIGHT / 2 + playerNameFont.getLineHeight() + 25);
            batch.end();
        }
        
        if (player.isHPDroping()) {
            playerNameFont.setColor(Color.RED);
            batch.begin();
            String skillName = "Dying!";
            playerNameFont.draw(batch, skillName, player.getBody().getPosition().x * PPM - player.getName().length() * playerNameFont.getSpaceWidth() - (skillName.length() * playerNameFont.getSpaceWidth()) / 2, player.getBody().getPosition().y * PPM + Player.SPRITE_HEIGHT / 2 + playerNameFont.getLineHeight() + 50);
            batch.end();
        }
        
        for (Player otherPlayer : otherPlayers.values()) {
            if (!otherPlayer.isCreatedAnimations()) otherPlayer.createAnimations();
            if (otherPlayer.getHP() > 0) otherPlayer.render(batch);
            
            if (otherPlayer.getHP() > 0) playerNameFont.setColor(otherPlayer.getTeamColor());
            if (otherPlayer.getHP() > 0) drawPlayerName(otherPlayer);
            
            if (otherPlayer.isSkill1Started()) {
                playerNameFont.setColor(Color.ORANGE);
                batch.begin();
                String skillName = "lol";
                if (otherPlayer.getTeam() == Team.HUMAN) skillName = "Dig Down";
                if (otherPlayer.getTeam() == Team.ZOMBIE) skillName = "Sprint";
                playerNameFont.draw(batch, skillName, otherPlayer.getBody().getPosition().x * PPM - otherPlayer.getName().length() * playerNameFont.getSpaceWidth() - (skillName.length() * playerNameFont.getSpaceWidth()) / 2, otherPlayer.getBody().getPosition().y * PPM + Player.SPRITE_HEIGHT / 2 + playerNameFont.getLineHeight() + 25);
                batch.end();
            }
            
            if (otherPlayer.isSkill2Started()) {
                playerNameFont.setColor(Color.ORANGE);
                batch.begin();
                String skillName = "lol";
                if (otherPlayer.getTeam() == Team.HUMAN) skillName = "Build Wall";
                if (otherPlayer.getTeam() == Team.ZOMBIE) skillName = "Blast Wall";
                playerNameFont.draw(batch, skillName, otherPlayer.getBody().getPosition().x * PPM - otherPlayer.getName().length() * playerNameFont.getSpaceWidth() - (skillName.length() * playerNameFont.getSpaceWidth()) / 2, otherPlayer.getBody().getPosition().y * PPM + Player.SPRITE_HEIGHT / 2 + playerNameFont.getLineHeight() + 25);
                batch.end();
            }
            
            if (otherPlayer.isHPDroping()) {
                playerNameFont.setColor(Color.RED);
                batch.begin();
                String skillName = "Dying!";
                playerNameFont.draw(batch, skillName, otherPlayer.getBody().getPosition().x * PPM - otherPlayer.getName().length() * playerNameFont.getSpaceWidth() - (skillName.length() * playerNameFont.getSpaceWidth()) / 2, otherPlayer.getBody().getPosition().y * PPM + Player.SPRITE_HEIGHT / 2 + playerNameFont.getLineHeight() + 50);
                batch.end();
            }
        }
        
        if (player.getHP() <= 0) {
            player.setHPDroping(false);
            if (otherPlayers.size() > 0) cameraPlayer = (Player) otherPlayers.values().toArray()[worldManager.cameraPlayerIndex];
        }
        
        if ((worldManager.getOtherPlayers().size() > 0 && worldManager.getAliveHumanCount() == 0) || (worldManager.getOtherPlayers().size() > 0 && worldManager.getAliveZombieCount() == 0) || (worldManager.getOtherPlayers().size() == 0 && player.getHP() <= 0)) {
            game.setScreen(new EndGameScreen(game));
        }
        
        for (Syringe syringe : syringes) {
            if (!syringe.isCreatedTexture()) syringe.createTexture();
            if (!syringe.isPickedUp()) syringe.render(batch);
        }
        
//        debugRenderer.render(world, box2DCamera.combined);
        
        hud.render();
    }
    
    @Override
    public void dispose() {
        hud.dispose();
        map.dispose();
        mapRenderer.dispose();
    }
    
    private void drawPlayerName(Player player) {
        batch.begin();
        playerNameFont.draw(batch, player.getName(), player.getBody().getPosition().x * PPM - player.getName().length() * playerNameFont.getSpaceWidth(), player.getBody().getPosition().y * PPM + Player.SPRITE_HEIGHT / 2 + playerNameFont.getLineHeight());
        batch.end();
    }
    
    private void createMap() {
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("dirt");
        
        float tileWidth = layer.getTileWidth();
        
        for (int row = 0; row < layer.getHeight(); row++) {
            for (int col = 0; col < layer.getWidth(); col++) {
                Cell cell = layer.getCell(col, row);
                if (cell == null || cell.getTile() == null) continue;
                
                BodyDef bodyDef = new BodyDef();
                bodyDef.position.set((col + 0.5f) * tileWidth / PPM, (row + 0.5f) * tileWidth / PPM);
                bodyDef.type = BodyType.StaticBody;
                
                ChainShape shape = new ChainShape();
                Vector2[] vertices = new Vector2[5];
                vertices[0] = new Vector2(-tileWidth / 2 / PPM, -tileWidth / 2 / PPM);
                vertices[1] = new Vector2(-tileWidth / 2 / PPM, tileWidth / 2 / PPM);
                vertices[2] = new Vector2(tileWidth / 2 / PPM, tileWidth / 2 / PPM);
                vertices[3] = new Vector2(tileWidth / 2 / PPM, -tileWidth / 2 / PPM);
                vertices[4] = new Vector2(-tileWidth / 2 / PPM, -tileWidth / 2 / PPM);
                shape.createChain(vertices);
                
                FixtureDef fixtureDef = new FixtureDef();
                fixtureDef.shape = shape;
                fixtureDef.filter.categoryBits = GROUND_BITS;
                fixtureDef.filter.maskBits = PLAYER_BITS;
                
                Body body = world.createBody(bodyDef);
                body.createFixture(fixtureDef);
                groundWall.add(body);
                
                shape.dispose();
            }
        }
        
        layer = (TiledMapTileLayer) map.getLayers().get("stone");
        
        tileWidth = layer.getTileWidth();
        
        for (int row = 0; row < layer.getHeight(); row++) {
            for (int col = 0; col < layer.getWidth(); col++) {
                Cell cell = layer.getCell(col, row);
                if (cell == null || cell.getTile() == null) continue;
                
                BodyDef bodyDef = new BodyDef();
                bodyDef.position.set((col + 0.5f) * tileWidth / PPM, (row + 0.5f) * tileWidth / PPM);
                bodyDef.type = BodyType.StaticBody;
                
                ChainShape shape = new ChainShape();
                Vector2[] vertices = new Vector2[5];
                vertices[0] = new Vector2(-tileWidth / 2 / PPM, -tileWidth / 2 / PPM);
                vertices[1] = new Vector2(-tileWidth / 2 / PPM, tileWidth / 2 / PPM);
                vertices[2] = new Vector2(tileWidth / 2 / PPM, tileWidth / 2 / PPM);
                vertices[3] = new Vector2(tileWidth / 2 / PPM, -tileWidth / 2 / PPM);
                vertices[4] = new Vector2(-tileWidth / 2 / PPM, -tileWidth / 2 / PPM);
                shape.createChain(vertices);
                
                FixtureDef fixtureDef = new FixtureDef();
                fixtureDef.shape = shape;
                fixtureDef.filter.categoryBits = WALL_BITS;
                fixtureDef.filter.maskBits = PLAYER_BITS;
                
                world.createBody(bodyDef).createFixture(fixtureDef);
                
                shape.dispose();
            }
        }
        
        layer = (TiledMapTileLayer) map.getLayers().get("ladder");
        
        tileWidth = layer.getTileWidth();
        
        for (int row = 0; row < layer.getHeight(); row++) {
            for (int col = 0; col < layer.getWidth(); col++) {
                Cell cell = layer.getCell(col, row);
                if (cell == null || cell.getTile() == null) continue;
                
                BodyDef bodyDef = new BodyDef();
                bodyDef.position.set((col + 0.5f) * tileWidth / PPM, (row + 0.5f) * tileWidth / PPM);
                bodyDef.type = BodyType.StaticBody;
                
                ChainShape shape = new ChainShape();
                Vector2[] vertices = new Vector2[5];
                vertices[0] = new Vector2(-tileWidth / 2 / PPM, -tileWidth / 2 / PPM);
                vertices[1] = new Vector2(-tileWidth / 2 / PPM, tileWidth / 2 / PPM);
                vertices[2] = new Vector2(tileWidth / 2 / PPM, tileWidth / 2 / PPM);
                vertices[3] = new Vector2(tileWidth / 2 / PPM, -tileWidth / 2 / PPM);
                vertices[4] = new Vector2(-tileWidth / 2 / PPM, -tileWidth / 2 / PPM);
                shape.createChain(vertices);
                
                FixtureDef fixtureDef = new FixtureDef();
                fixtureDef.shape = shape;
                fixtureDef.filter.categoryBits = LADDER_BITS;
                fixtureDef.filter.maskBits = PLAYER_BITS;
                fixtureDef.isSensor = true;
                
                world.createBody(bodyDef).createFixture(fixtureDef).setUserData("ladder");
                
                shape.dispose();
            }
        }
        
        MapLayer syringesLayer = map.getLayers().get("syringes");
        
        for (int i = 0; i < syringesLayer.getObjects().getCount(); i++) {
            float x = (Float) syringesLayer.getObjects().get(i).getProperties().get("x") / PPM;
            float y = (Float) syringesLayer.getObjects().get(i).getProperties().get("y") / PPM;
            
            Syringe syringe = new Syringe("Syringe " + i, x, y);
            
            Body body = world.createBody(syringe.getBodyDef());
            body.createFixture(syringe.getFixtureDef()).setUserData(syringe);
            body.createFixture(syringe.getRadiusFixtureDef()).setUserData("syringeRadius");
            
            syringe.setBody(body);
            syringes.add(syringe);
        }
        
        MapLayer spawnPointsLayer = map.getLayers().get("spawnPoints");
        
        List<Vector2> spawnPoints = new ArrayList<Vector2>();
        
        for (int i = 0; i < spawnPointsLayer.getObjects().getCount(); i++) {
            float x = (Float) spawnPointsLayer.getObjects().get(i).getProperties().get("x") / PPM;
            float y = (Float) spawnPointsLayer.getObjects().get(i).getProperties().get("y") / PPM;
            spawnPoints.add(new Vector2(x, y));
        }
        
        Collections.shuffle(spawnPoints);
        
        
        
        int playerCount=1;
        
        if (!world.isLocked()) player.getBody().setTransform(spawnPoints.get(0), 0);
        
//        for(Player otherPlayer: otherPlayers.values()){
//        	otherPlayer.getBody().setTransform(spawnPoints.get(playerCount), 0);
//        	Gdx.app.log(TAG, String.valueOf(spawnPoints.get(playerCount).x)+" "+String.valueOf(spawnPoints.get(playerCount).y));
//        	Gdx.app.log(TAG, otherPlayer.getName()+"5555555555555555555 "+String.valueOf(otherPlayer.getBody().getPosition().x)+" "+String.valueOf(otherPlayer.getBody().getPosition().y));
//        	playerCount++;
//        	
//        }
        
        SetPositionPacket setPacket = new SetPositionPacket();
		setPacket.setX(player.getBody().getPosition().x);
		setPacket.setY(player.getBody().getPosition().y);
		networkManager.send(setPacket);
        
    }
    
    private boolean intersectRect(double right1, double left1, double top1, double bottom1, double right2, double left2, double top2, double bottom2) {
        return !(left2 > right1 || right2 < left1 || top2 < bottom1 || bottom2 > top1);
    }
    
    private boolean buildWall(float x, float y) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(x, y);
        bodyDef.type = BodyType.StaticBody;
        
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(25 / PPM, 46 / PPM);
        
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = HUMAN_WALL_BITS;
        fixtureDef.filter.maskBits = PLAYER_BITS;
        
        Body body = world.createBody(bodyDef);
        body.createFixture(fixtureDef);
        humanWall.add(body);
        
        shape.dispose();
        
        return true;
    }
    
    private boolean blastWall(Player player) {
        switch (player.getDirection()) {
        case RIGHT:
            for (int i = 0; i < humanWall.size(); i++) {
                Body body = humanWall.get(i);
                if (intersectRect((body.getPosition().x * PPM) + 25, (body.getPosition().x * PPM) - 25, (body.getPosition().y * PPM) + 50, (body.getPosition().y * PPM) - 50, (player.getBody().getPosition().x * PPM) + 55, (player.getBody().getPosition().x * PPM), (player.getBody().getPosition().y * PPM) + 50, (player.getBody().getPosition().y * PPM) - 50)) {
                    replaceHumanWall.add(body);
                    if (!world.isLocked()) world.destroyBody(body);
                    humanWall.remove(i);i--;
                    
                }
            }
            
//            for (Body body : groundWall) {
//                if (intersectRect((body.getPosition().x * PPM) + 25, (body.getPosition().x * PPM) - 25, (body.getPosition().y * PPM) + 25, (body.getPosition().y * PPM) - 25, (player.getBody().getPosition().x * PPM) + 55, (player.getBody().getPosition().x * PPM), (player.getBody().getPosition().y * PPM) + 50, (player.getBody().getPosition().y * PPM) - 50)) {
//                    replaceWall.add(body);
//                    if (!world.isLocked()) world.destroyBody(body);
//                    
//                }
//            }
            
            return true;
            
        case LEFT:
            for (int i = 0; i < humanWall.size(); i++) {
                Body body = humanWall.get(i);
                if (intersectRect((body.getPosition().x * PPM) + 25, (body.getPosition().x * PPM) - 25, (body.getPosition().y * PPM) + 50, (body.getPosition().y * PPM) - 50, (player.getBody().getPosition().x * PPM), (player.getBody().getPosition().x * PPM) - 55, (player.getBody().getPosition().y * PPM) + 50, (player.getBody().getPosition().y * PPM) - 50)) {
                    replaceHumanWall.add(body);
                    if (!world.isLocked()) world.destroyBody(body);
                    humanWall.remove(i);i--;
                    
                }
            }
            
//            for (Body body : groundWall) {
//                if (intersectRect((body.getPosition().x * PPM) + 25, (body.getPosition().x * PPM) - 25, (body.getPosition().y * PPM) + 25, (body.getPosition().y * PPM) - 25, (player.getBody().getPosition().x * PPM), (player.getBody().getPosition().x * PPM) - 55, (player.getBody().getPosition().y * PPM) + 50, (player.getBody().getPosition().y * PPM) - 50)) {
//                    replaceWall.add(body);
//                    if (!world.isLocked()) world.destroyBody(body);
//                    
//                }
//            }
            
            return true;
            
        case UP:
            return true;
        }
        return false;
    }
    
    private boolean isSkill2Done(Player player) {
        switch (player.getTeam()) {
        case HUMAN:
            if (player.isSkill2Started()) switch (player.getDirection()) {
            case RIGHT:
                return buildWall(player.getBody().getPosition().x - 1, player.getBody().getPosition().y);
            case LEFT:
                return buildWall(player.getBody().getPosition().x + 1, player.getBody().getPosition().y);
            case UP:
                break;
            }
            
        case ZOMBIE:
            if (player.isSkill2Started()) { return blastWall(player); }
        }
        
        return false;
        
    }
    
    private boolean isSkill1Done(Player player) {
        switch (player.getTeam()) {
        case HUMAN:
            return ((System.currentTimeMillis() - player.getSkill1StartTime()) >= 500) && player.isSkill1Started();
            
        case ZOMBIE:
            return ((System.currentTimeMillis() - player.getSkill1StartTime()) >= 5000) && player.isSkill1Started();
        }
        return false;
        
    }
    
    private boolean isSkill1Ready(Player player) {
        switch (player.getTeam()) {
        case HUMAN:
            return ((System.currentTimeMillis() - player.getSkill1StartTime()) >= 10000);
            
        case ZOMBIE:
            return ((System.currentTimeMillis() - player.getSkill1StartTime()) >= 20000);
        default:
            return false;
        }
        
    }
    
    private boolean isSkill2Ready(Player player) {
        switch (player.getTeam()) {
        case HUMAN:
            return ((System.currentTimeMillis() - player.getSkill2StartTime()) >= 10000);
            
        case ZOMBIE:
            return ((System.currentTimeMillis() - player.getSkill2StartTime()) >= 15000);
        default:
            return false;
        }
    }
    
}
