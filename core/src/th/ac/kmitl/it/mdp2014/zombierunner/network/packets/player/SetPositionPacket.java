package th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player;

import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.Packet;


public class SetPositionPacket extends Packet {
	
	private float x;
	private float y;
	
	public float getX() {
		return x;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setY(float y) {
		this.y = y;
	}

}
