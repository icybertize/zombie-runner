package th.ac.kmitl.it.mdp2014.zombierunner.listeners;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Player;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Syringe;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetTeamPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetTeamPacket.Team;
import th.ac.kmitl.it.mdp2014.zombierunner.screens.EndGameScreen;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class WorldContactListener implements ContactListener {
	
	private ZombieRunner game;
	
	private WorldManager worldManager;
	private NetworkManager networkManager;
	
	private int ladderContactCount;
    private int syringeRadiusContactCount;
    private int playerContactCount;
	
	public WorldContactListener(ZombieRunner game) {
		this.game = game;
		
		worldManager = game.getWorldManager();
		networkManager = game.getNetworkManager();
		
		ladderContactCount = 0;
		syringeRadiusContactCount = 0;
		playerContactCount = 0;
	}

	@Override
	public void beginContact(Contact contact) {
		Object fixtureAUserData = contact.getFixtureA().getUserData();
        Object fixtureBUserData = contact.getFixtureB().getUserData();
        
        if (fixtureAUserData == null || fixtureBUserData == null) return;
		
		if ((fixtureAUserData instanceof Player && fixtureBUserData.equals("ladder")) ||
		    (fixtureBUserData instanceof Player && fixtureAUserData.equals("ladder")))
		{
		    ladderContactCount++;
		}
		
		checkForLadder(contact);
        
        if ((fixtureAUserData instanceof Player && fixtureBUserData instanceof Syringe) ||
            (fixtureBUserData instanceof Player && fixtureAUserData instanceof Syringe))
        {
            Player player = null;
            Syringe syringe = null;
            if (fixtureBUserData instanceof Player) player = (Player) fixtureBUserData;
            if (fixtureAUserData instanceof Player) player = (Player) fixtureAUserData;
            if (fixtureBUserData instanceof Syringe) syringe = (Syringe) fixtureBUserData;
            if (fixtureAUserData instanceof Syringe) syringe = (Syringe) fixtureAUserData;
            if (player.getTeam() == SetTeamPacket.Team.HUMAN) syringe.setPickedUp(true);
            
            if (worldManager.getCollectedSyringes() == worldManager.getSyringes().size()) {
                if (networkManager.isServer()) {
                    networkManager.closeServer();
                } else {
                    networkManager.disconnect();
                }
                game.setScreen(new EndGameScreen(game));
            }
        }
        
        if ((fixtureAUserData instanceof Player && fixtureBUserData.equals("syringeRadius")) ||
            (fixtureBUserData instanceof Player && fixtureAUserData.equals("syringeRadius")))
        {
            syringeRadiusContactCount++;
        }
        
        checkForSyringeRadius(contact);
        
        if ((fixtureAUserData instanceof Player && fixtureBUserData instanceof Player) ||
            (fixtureBUserData instanceof Player && fixtureAUserData instanceof Player))
        {
            playerContactCount++;
            System.out.println("NANANANANANANANANANANA");
        }
        
        checkForPlayer(contact);
	}

	@Override
	public void endContact(Contact contact) {
        Object fixtureAUserData = contact.getFixtureA().getUserData();
        Object fixtureBUserData = contact.getFixtureB().getUserData();
        
        if (fixtureAUserData == null || fixtureBUserData == null) return;
		
        if ((fixtureAUserData instanceof Player && fixtureBUserData.equals("ladder")) ||
            (fixtureBUserData instanceof Player && fixtureAUserData.equals("ladder")))
        {
            ladderContactCount--;
        }
		
		checkForLadder(contact);
        
        if ((fixtureAUserData instanceof Player && fixtureBUserData.equals("syringeRadius")) ||
            (fixtureBUserData instanceof Player && fixtureAUserData.equals("syringeRadius")))
        {
            syringeRadiusContactCount--;
        }
        
        checkForSyringeRadius(contact);
        
        if ((fixtureAUserData instanceof Player && fixtureBUserData instanceof Player) ||
            (fixtureBUserData instanceof Player && fixtureAUserData instanceof Player))
        {
            playerContactCount--;
            System.out.println("NANANANANANANANANANANA");
        }
        
        checkForPlayer(contact);
	}
    
    private void checkForPlayer(Contact contact) {
        Object fixtureAUserData = contact.getFixtureA().getUserData();
        Object fixtureBUserData = contact.getFixtureB().getUserData();
        
        if ((fixtureAUserData instanceof Player && fixtureBUserData instanceof Player) ||
            (fixtureBUserData instanceof Player && fixtureAUserData instanceof Player))
        {
            Player playerA = (Player) fixtureAUserData;
            Player playerB = (Player) fixtureBUserData;
            if (playerContactCount > 0) {
                if (playerA.getTeam() == Team.HUMAN && playerB.getTeam() == Team.ZOMBIE && playerB.getHP() > 0) {
                    System.out.println("BABABABABABABABABABABABABABAB");
                    playerA.setHPDroping(true);
                }
                else if (playerB.getTeam() == Team.HUMAN && playerA.getTeam() == Team.ZOMBIE && playerA.getHP() > 0) {
                    System.out.println("BABABABABABABABABABABABABABAB");
                    playerB.setHPDroping(true);
                }
            }
        }
        
    }
	
	private void checkForSyringeRadius(Contact contact) {
	    Object fixtureAUserData = contact.getFixtureA().getUserData();
        Object fixtureBUserData = contact.getFixtureB().getUserData();
	    Player player = null;
	    Syringe syringe = null;
	    System.out.println(contact.getFixtureA().getBody().getFixtureList().size);
        System.out.println(contact.getFixtureB().getBody().getFixtureList().size);
        System.out.println(syringeRadiusContactCount);
	    Object xxx = contact.getFixtureA().getBody().getFixtureList().get(0).getUserData();
        Object yyy = contact.getFixtureB().getBody().getFixtureList().get(0).getUserData();
        System.out.println(xxx.getClass());
        System.out.println(yyy.getClass());
        if (fixtureBUserData instanceof Player) player = (Player) fixtureBUserData;
        if (fixtureAUserData instanceof Player) player = (Player) fixtureAUserData;
        if (xxx instanceof Syringe) syringe = (Syringe) xxx;
        if (yyy instanceof Syringe) syringe = (Syringe) yyy;
        
        if (syringeRadiusContactCount > 0) {
            if (syringe != null && !syringe.isPickedUp()) {
                if (player.getTeam() == Team.ZOMBIE) player.setHPDroping(true);
                if (player.getTeam() == Team.HUMAN) player.setHPDroping(false);
            }
        } else {
            if (syringe != null && !syringe.isPickedUp()) {
                if (player.getTeam() == Team.ZOMBIE) player.setHPDroping(false);
                if (player.getTeam() == Team.HUMAN) player.setHPDroping(false);
            }
        }
	}

    private void checkForLadder(Contact contact) {
        Object fixtureAUserData = contact.getFixtureA().getUserData();
        Object fixtureBUserData = contact.getFixtureB().getUserData();
        Player player = null;
        if (fixtureBUserData instanceof Player) player = (Player) fixtureBUserData;
        if (fixtureAUserData instanceof Player) player = (Player) fixtureAUserData;
        if (ladderContactCount > 0) {
            player.setWalkUpDownAble(true);
        } else {
            player.setWalkUpDownAble(false);
            if (player.isWalkingUp() && !player.isWalkingLeftRight()) { player.stopWalking(); }
        }
    }

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		
	}

}
