package th.ac.kmitl.it.mdp2014.zombierunner.utils;

public enum Map {
    BASIC("basic"),
    BACK_BONE("backBone"),
    COMPLEX("complex");
    
    private String name;

    Map(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
}
