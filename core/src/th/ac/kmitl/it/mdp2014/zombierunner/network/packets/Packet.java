package th.ac.kmitl.it.mdp2014.zombierunner.network.packets;

public abstract class Packet {
	
	private Integer connectionID;

	public Integer getConnectionID() {
		return connectionID;
	}

	public void setConnectionID(Integer connectionID) {
		this.connectionID = connectionID;
	}

}
