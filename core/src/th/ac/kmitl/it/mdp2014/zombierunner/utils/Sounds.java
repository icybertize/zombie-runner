package th.ac.kmitl.it.mdp2014.zombierunner.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class Sounds {
    
    private boolean isPlaySound;
    
    public Sound menuMusic;
    public Sound inGameMusic;
    
    public Sound clickSound;
    
    public Sound blastWallSound;
    public Sound buildWallSound;
    public Sound digDownSound;
    public Sound pickSyringeSound;
    public Sound doSprintSound;
    
    public Sounds() {
        isPlaySound = true;
        
        clickSound = Gdx.audio.newSound(Gdx.files.internal("sounds/ui/click.mp3"));
        
        menuMusic = clickSound;
        inGameMusic = clickSound;
        
        blastWallSound = clickSound;
        buildWallSound = clickSound;
        digDownSound = clickSound;
        pickSyringeSound = clickSound;
        doSprintSound = clickSound;
    }
    
    public void play(Sound sound) {
//        if (isPlaySound) sound.play();
    }
    
    public void loop(Sound sound) {
//        if (isPlaySound) sound.loop();
    }
    
    public void dispose() {
//        menuMusic.dispose();
//        inGameMusic.dispose();
        
        clickSound.dispose();
        
//        blastWallSound.dispose();
//        buildWallSound.dispose();
//        digDownSound.dispose();
//        pickSyringeSound.dispose();
//        doSprintSound.dispose();
    }
    
    public void setPlaySound(boolean isPlaySound) {
        this.isPlaySound = isPlaySound;
    }
}
