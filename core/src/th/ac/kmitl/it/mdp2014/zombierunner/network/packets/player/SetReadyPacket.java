package th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player;

import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.Packet;

public class SetReadyPacket extends Packet {

	private boolean ready;

	public boolean isReady() {
		return ready;
	}

	public void setReady(boolean value) {
		this.ready = value;
	}
	
}
