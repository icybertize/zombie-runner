package th.ac.kmitl.it.mdp2014.zombierunner.listeners;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.HelloPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.server.ServerInfoPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.RemovePlayerPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.SetMapPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.HostInfo;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.minlog.Log;

public class ClientListener extends CommonListener {
	
	private static final String TAG = "CLIENT";
	
	public ClientListener(ZombieRunner game) {
		super(game);
	}

	@Override
	public void connected(Connection connection) {
		HelloPacket packet = new HelloPacket();
		packet.setMessage("Hello Server!");
		connection.sendUDP(packet);
	}

	@Override
	public void disconnected(Connection connection) {
		RemovePlayerPacket packet = new RemovePlayerPacket();
		packet.setConnectionID(connection.getID());
		networkManager.send(packet);
	}

	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof HelloPacket) {
			HelloPacket packet = (HelloPacket) object;
			
			Log.info(TAG, packet.getMessage() + " [FROM] " + connection.toString());
		}
		
		if (object instanceof SetMapPacket) {
            SetMapPacket packet = (SetMapPacket) object;
            
            worldManager.setMap(packet.getMap());
            
            Log.info(TAG, "SetMapPacket [FROM] " + connection.toString());
        }
        
        if (object instanceof ServerInfoPacket) {
            ServerInfoPacket packet = (ServerInfoPacket) object;
            
            worldManager.getManualJoinScreen().hostInfo = new HostInfo(connection.getRemoteAddressTCP().getAddress(), packet.getName(), packet.getMap(), packet.getPlayers());
            worldManager.getManualJoinScreen().leLabel.setText("Server Info: " + packet.getName() + ", " + packet.getMap() + " ("+packet.getPlayers()+" Players)");
            
            networkManager.disconnect();
            
            Log.info(TAG, "ServerInfoPacket [FROM] " + connection.toString());
        }
		
		super.received(connection, object);
	}

	@Override
	public void idle(Connection connection) {

	}

}
