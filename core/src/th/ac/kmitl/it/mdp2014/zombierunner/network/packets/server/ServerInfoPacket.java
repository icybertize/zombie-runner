package th.ac.kmitl.it.mdp2014.zombierunner.network.packets.server;

import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.Packet;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.Map;


public class ServerInfoPacket extends Packet {
    
    private String name;
    private Map map;
    private int players;
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Map getMap() {
        return map;
    }
    
    public void setMap(Map map) {
        this.map = map;
    }
    
    public int getPlayers() {
        return players;
    }
    
    public void setPlayers(int players) {
        this.players = players;
    }
}
