package th.ac.kmitl.it.mdp2014.zombierunner.utils;

import java.net.InetAddress;


public class HostInfo {
    private InetAddress address;
    private String name;
    private Map map;
    private int players;
    public HostInfo(InetAddress address, String name, Map map, int players) {
        super();
        this.address = address;
        this.name = name;
        this.map = map;
        this.players = players;
    }
    
    public InetAddress getAddress() {
        return address;
    }
    
    public void setAddress(InetAddress address) {
        this.address = address;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Map getMap() {
        return map;
    }
    
    public void setMap(Map map) {
        this.map = map;
    }
    
    public int getPlayers() {
        return players;
    }
    
    public void setPlayers(int players) {
        this.players = players;
    }
}
