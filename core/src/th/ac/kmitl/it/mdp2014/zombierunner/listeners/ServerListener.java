package th.ac.kmitl.it.mdp2014.zombierunner.listeners;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.HelloPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.server.RequestServerInfoPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.server.ServerInfoPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.CreatePlayerPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.RemovePlayerPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.RequestOtherPlayersPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.SetMapPacket;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.minlog.Log;

public class ServerListener extends CommonListener {
	
	private static final String TAG = "SERVER";
	
	public ServerListener(ZombieRunner game) {
		super(game);
	}

	@Override
	public void connected(Connection connection) {
		SetMapPacket packet = new SetMapPacket();
		packet.setMap(worldManager.getMap());
		networkManager.send(packet);
	}

	@Override
	public void disconnected(Connection connection) {
		worldManager.removePlayer(connection.getID());
		RemovePlayerPacket packet = new RemovePlayerPacket();
		packet.setConnectionID(connection.getID());
		networkManager.send(packet);
	}

	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof HelloPacket) {
			HelloPacket packet = (HelloPacket) object;
			
			Log.info(TAG, "HelloPacket:" + packet.getMessage() + " [FROM] " + connection.toString());
			
			packet = new HelloPacket();
			packet.setMessage("Hello Client!");
			connection.sendUDP(packet);
		}
		
		if (object instanceof RequestOtherPlayersPacket) {
			Log.info(TAG, "RequestOtherPlayers [FROM] " + connection.toString());
			
			CreatePlayerPacket me = new CreatePlayerPacket();
			me.setName(worldManager.getPlayer().getName());
			me.setX(worldManager.getPlayer().getBody().getPosition().x);
			me.setY(worldManager.getPlayer().getBody().getPosition().y);
			me.setSkinName(worldManager.getPlayer().getSkinName());
			me.setTeam(worldManager.getPlayer().getTeam());
			connection.sendUDP(me);
			System.out.println(me);
			
			Object[] keys = otherPlayers.keySet().toArray();
			
			for (int i = 0; i < keys.length; i++) {
				int key = (Integer) keys[i];
				
				CreatePlayerPacket packet = new CreatePlayerPacket();
				packet.setConnectionID(key);
				packet.setName(otherPlayers.get(key).getName());
				packet.setX(otherPlayers.get(key).getBody().getPosition().x);
				packet.setY(otherPlayers.get(key).getBody().getPosition().y);
				packet.setSkinName(otherPlayers.get(key).getSkinName());
				packet.setTeam(otherPlayers.get(key).getTeam());
				connection.sendUDP(packet);
			}
		}
		
		if (object instanceof RequestServerInfoPacket) {
		    ServerInfoPacket packet = new ServerInfoPacket();
		    packet.setName(worldManager.getPlayer().getName());
		    packet.setMap(worldManager.getMap());
		    packet.setPlayers(1 + worldManager.getOtherPlayers().size());
		    connection.sendUDP(packet);
		}
		
		super.received(connection, object);
	}

	@Override
	public void idle(Connection connection) {

	}

}
