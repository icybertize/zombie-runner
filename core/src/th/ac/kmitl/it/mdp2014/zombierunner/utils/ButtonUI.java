package th.ac.kmitl.it.mdp2014.zombierunner.utils;

import java.util.ArrayList;
import java.util.List;

import th.ac.kmitl.it.mdp2014.zombierunner.generators.FontGenerator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class ButtonUI {
	
	private Stage stage;
	
	private Skin skin;
	
	private BitmapFont font;
	
	private Table table;
	
	private List<ButtonData> buttons;
	
	public ButtonUI() {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		skin = new Skin();
		
		table = new Table();
		table.setFillParent(true);
		table.right();
		stage.addActor(table);
		
		buttons = new ArrayList<ButtonData>();
	}
	
	public void render() {
		stage.act();
		stage.draw();
	}
	
	public void dispose() {
		stage.dispose();
		skin.dispose();
	}
	
	public void create() {
		createButtons();
	}
	
	public void createButtons() {
		int buttonCount = Math.max(buttons.size(), 4);
		int buttonFontSize = Gdx.graphics.getHeight() / (buttonCount * 2);
		
		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("white", new Texture(pixmap));
		
		font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
		skin.add("default", font);
		
		TextButtonStyle style = new TextButtonStyle();
		style.fontColor = Color.BLACK;
		style.font = skin.getFont("default");
		style.up = skin.newDrawable("white", Color.WHITE);
		style.down = skin.newDrawable("white", Color.RED);
		style.downFontColor = Color.WHITE;
		skin.add("default", style);
		
		for (ButtonData data : buttons) {
			TextButton button = new TextButton(data.text, skin);
			button.padLeft(buttonFontSize / 3).padRight(buttonFontSize / 3);
			button.addListener(data.listener);
			table.add(button).right().padBottom(buttonFontSize / 3);
			table.row();
		}
	}
	
	public void addButton(String text, EventListener listener) {
		buttons.add(new ButtonData(text, listener));
	}
	
	public Stage getStage() {
		return stage;
	}
	
	public BitmapFont getFont() {
		return font;
	}
	
	public Skin getSkin() {
		return skin;
	}

	private class ButtonData {
		
		private String text;
		private EventListener listener;
		
		private ButtonData(String text, EventListener listener) {
			this.text = text;
			this.listener = listener;
		}
		
	}

}
