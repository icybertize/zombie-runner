package th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world;

import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.Packet;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.Map;

public class SetMapPacket extends Packet {
    
    private Map map;
    
    public Map getMap() {
        return map;
    }
    
    public void setMap(Map map) {
        this.map = map;
    }
}
