package th.ac.kmitl.it.mdp2014.zombierunner.listeners;

import java.util.HashMap;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Player;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetPositionPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetReadyPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetSkinNamePacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetTeamPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.DoSkill1Packet;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.DoSkill2Packet;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.StopWalkingPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkDownPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkLeftPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkRightPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkUpPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.CreatePlayerPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.RemovePlayerPacket;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.minlog.Log;

public abstract class CommonListener extends Listener {
    
    private static final String TAG = "COMMON";
    
    protected ZombieRunner game;
    
    protected WorldManager worldManager;
    protected HashMap<Integer, Player> otherPlayers;
    
    protected NetworkManager networkManager;
    
    protected CommonListener(ZombieRunner game) {
        this.game = game;
        
        worldManager = game.getWorldManager();
        otherPlayers = worldManager.getOtherPlayers();
        
        networkManager = game.getNetworkManager();
    }
    
    @Override
    public void received(Connection connection, Object object) {
        if (object instanceof CreatePlayerPacket) {
            CreatePlayerPacket packet = (CreatePlayerPacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = new Player(packet.getName(), packet.getX(), packet.getY(), packet.getSkinName(), packet.getTeam());
            worldManager.createPlayer(otherPlayer);
            otherPlayers.put(packet.getConnectionID(), otherPlayer);
            System.out.println(packet);
            
            System.out.println("-------------------------");
            for (int id : otherPlayers.keySet()) {
                System.out.println(id + " " + otherPlayers.get(id).getName());
            }
            System.out.println("-------------------------");
            System.out.println(otherPlayers.size());
            
            worldManager.getLobbyScreen().shit();System.out.println("sdflskdajfnsdljfdlsfldsflj");
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "CreatePlayerPacket: " + packet.getName() + " [FROM] " + connection.toString());
        }
        
        if (object instanceof RemovePlayerPacket) {
            RemovePlayerPacket packet = (RemovePlayerPacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            worldManager.removePlayer(packet.getConnectionID());
            System.out.println("SLFJSDFOISDJOFJ " + packet.getConnectionID());
            worldManager.getLobbyScreen().shit();
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "RemovePlayerPacket: " + packet.getConnectionID() + " [FROM] " + connection.toString());
        }
        
        if (object instanceof SetTeamPacket) {
            SetTeamPacket packet = (SetTeamPacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            otherPlayer.setTeam(packet.getTeam());
            
            worldManager.getLobbyScreen().shit();
            System.out.println("sdflskdajfnsdljfdlsfldsflj");
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "SetTeamPacket [FROM] " + otherPlayer.getName() + " -> " + packet.getTeam());
        }
        
        if (object instanceof SetSkinNamePacket) {
            SetSkinNamePacket packet = (SetSkinNamePacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            otherPlayer.setSkinName(packet.getSkinName());
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "SetSkinNamePacket [FROM] " + otherPlayer.getName() + " -> " + packet.getSkinName());
        }
        
        if (object instanceof SetReadyPacket) {
            SetReadyPacket packet = (SetReadyPacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            otherPlayer.setReady(packet.isReady());
            
            worldManager.getLobbyScreen().shit();System.out.println("4u54894528529852");
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "SetReadyPacket [FROM] " + otherPlayer.getName() + " -> " + packet.isReady());
        }
        
        if (object instanceof SetPositionPacket) {
            SetPositionPacket packet = (SetPositionPacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            if (!worldManager.getWorld().isLocked())
                otherPlayer.getBody().setTransform(packet.getX(), packet.getY(), 0);
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "SetPositionPacket [FROM] " + otherPlayer.getName());
        }
        
        if (object instanceof WalkUpPacket) {
            WalkUpPacket packet = (WalkUpPacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            otherPlayer.walkUp();
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "WalkUpPacket [FROM] " + otherPlayer.getName());
        }
        
        if (object instanceof WalkDownPacket) {
            WalkDownPacket packet = (WalkDownPacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            otherPlayer.walkDown();
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "WalkDownPacket [FROM] " + otherPlayer.getName());
        }
        
        if (object instanceof WalkLeftPacket) {
            WalkLeftPacket packet = (WalkLeftPacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            otherPlayer.walkLeft();
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "WalkLeftPacket [FROM] " + otherPlayer.getName());
        }
        
        if (object instanceof WalkRightPacket) {
            WalkRightPacket packet = (WalkRightPacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            otherPlayer.walkRight();
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "WalkRightPacket [FROM] " + otherPlayer.getName());
        }
        
        if (object instanceof StopWalkingPacket) {
            StopWalkingPacket packet = (StopWalkingPacket) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            otherPlayer.stopWalking();
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "StopWalkingPacket [FROM] " + otherPlayer.getName());
        }
        
        if (object instanceof DoSkill1Packet) {
            DoSkill1Packet packet = (DoSkill1Packet) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            otherPlayer.doSkill1();
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "DoSkill1Packet [FROM] " + otherPlayer.getName());
        }
        
        if (object instanceof DoSkill2Packet) {
            DoSkill2Packet packet = (DoSkill2Packet) object;
            if (packet.getConnectionID() == null) packet.setConnectionID(connection.getID());
            
            Player otherPlayer = otherPlayers.get(packet.getConnectionID());
            otherPlayer.doSkill2();
            
            if (networkManager.isServer()) networkManager.sendToOtherClients(packet.getConnectionID(), packet);
            
            Log.info(TAG, "DoSkill2Packet [FROM] " + otherPlayer.getName());
        }
    }
    
}
