package th.ac.kmitl.it.mdp2014.zombierunner.inputprocessors;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Player;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetPositionPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.StopWalkingPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkLeftPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkRightPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkUpPacket;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

public class PlayerInputProcessor implements InputProcessor {

	@SuppressWarnings("unused")
	private ZombieRunner game;

	private WorldManager worldManager;
	private Player player;

	private NetworkManager networkManager;

	private int keyPressedCount;

	public PlayerInputProcessor(ZombieRunner game) {
		this.game = game;

		worldManager = game.getWorldManager();
		player = worldManager.getPlayer();

		networkManager = game.getNetworkManager();

		keyPressedCount = 0;
	}

	@Override
	public boolean keyDown(int keycode) {
	    if (player.getHP() > 0) {
    		if (keycode == Keys.W) {
    			player.walkUp();
    			keyPressedCount++;
    
    			WalkUpPacket packet = new WalkUpPacket();
    			networkManager.send(packet);
    		}
    
    		// if (keycode == Keys.S) {
    		// player.walkDown();
    		// keyPressedCount++;
    		//
    		// WalkDownPacket packet = new WalkDownPacket();
    		// networkManager.send(packet);
    		// }
    
    		if (keycode == Keys.A) {
    			player.walkLeft();
    			keyPressedCount++;
    
                WalkLeftPacket packet = new WalkLeftPacket();
                networkManager.send(packet);
    		}
    
    		if (keycode == Keys.D) {
    			player.walkRight();
    			keyPressedCount++;
    
                WalkRightPacket packet = new WalkRightPacket();
                networkManager.send(packet);
    		}
	    }

		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.W || /* keycode == Keys.S || */keycode == Keys.A
				|| keycode == Keys.D) {
			keyPressedCount--;

			if (keyPressedCount == 0) {
				player.stopWalking();

				StopWalkingPacket packet = new StopWalkingPacket();
				networkManager.send(packet);


				SetPositionPacket setPacket = new SetPositionPacket();
				setPacket.setX(player.getBody().getPosition().x);
				setPacket.setY(player.getBody().getPosition().y);
				networkManager.send(setPacket);

			}
		}

		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
