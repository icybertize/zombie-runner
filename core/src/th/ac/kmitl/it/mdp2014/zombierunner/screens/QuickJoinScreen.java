package th.ac.kmitl.it.mdp2014.zombierunner.screens;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.generators.FontGenerator;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.ButtonUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class QuickJoinScreen extends ScreenAdapter {
	
	private static final String TAG = "QuickJoinScreen";
	
	private ZombieRunner game;
	
	private NetworkManager networkManager;
	
	private ButtonUI buttons;
	
	public QuickJoinScreen(ZombieRunner game) {
		this.game = game;
		
		networkManager = game.getNetworkManager();
		
		createUI();
	}
	
	@Override
	public void show() {
		Gdx.app.log(TAG, "show()");
        
        quickJoin();
	}
	
	private void quickJoin() {
		try {
			networkManager.quickJoin();
			game.setScreen(new LobbyScreen(game));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		buttons.render();
	}
	
	private void createUI() {
		buttons = new ButtonUI();
		
		int buttonFontSize = Gdx.graphics.getHeight() / 8;
		BitmapFont font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = font;
        buttons.getSkin().add("default", labelStyle);
        
		Label label = new Label("Can't connect to server.", buttons.getSkin());
        label.setPosition(buttonFontSize / 2, Gdx.graphics.getHeight() - label.getHeight() - buttonFontSize / 2);
        buttons.getStage().addActor(label);
		
		buttons.addButton("Try Again", new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
				quickJoin();
			}
		});
		
		buttons.addButton("Cancel", new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
				game.setScreen(new MainMenuScreen(game));
			}
		});
		
		buttons.create();
	}

}
