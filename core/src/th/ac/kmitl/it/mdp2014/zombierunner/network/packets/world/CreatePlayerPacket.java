package th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world;

import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.Packet;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetSkinNamePacket.SkinName;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetTeamPacket.Team;

public class CreatePlayerPacket extends Packet {
	
	protected String name;
	protected float x;
	protected float y;
	protected SkinName skinName;
	protected Team team;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public SkinName getSkinName() {
		return skinName;
	}

	public void setSkinName(SkinName skinName) {
		this.skinName = skinName;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

    @Override
    public String toString() {
        return "CreatePlayerPacket [name=" + name + ", x=" + x + ", y=" + y + ", skinName=" + skinName + ", team=" + team + "]";
    }

}
