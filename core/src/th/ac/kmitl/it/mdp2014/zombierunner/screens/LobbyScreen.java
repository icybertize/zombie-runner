package th.ac.kmitl.it.mdp2014.zombierunner.screens;

import java.util.HashMap;
import java.util.List;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Player;
import th.ac.kmitl.it.mdp2014.zombierunner.generators.FontGenerator;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetReadyPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetSkinNamePacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetSkinNamePacket.SkinName;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetTeamPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetTeamPacket.Team;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.CreatePlayerPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.RemovePlayerPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.world.RequestOtherPlayersPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.ButtonUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class LobbyScreen extends ScreenAdapter {
	
	private static final String TAG = "LobbyScreen";
	
	private ZombieRunner game;
	
	private WorldManager worldManager;
	private NetworkManager networkManager;
	
	private Player player;
	private HashMap<Integer, Player> otherPlayers;
	
	private ButtonUI buttons;
	
	private Label labelA;
	private Label labelB;
	
	private Label[] humansLabel;
    private Label[] zombiesLabel;
	
	private Preferences preferences;
	
	public LobbyScreen(ZombieRunner game) {
		this.game = game;
		
		worldManager = game.getWorldManager();
		networkManager = game.getNetworkManager();
		
		player = worldManager.getPlayer();
		otherPlayers = worldManager.getOtherPlayers();
		
		preferences = Gdx.app.getPreferences(ZombieRunner.PREFERENCES_FILE_NAME);
		
		createUI();
        
        createLobbyUI();
        worldManager.setLobbyScreen(this);
        System.out.println(worldManager.getLobbyScreen());
        
 
		
		createPlayer();
	}
	
	@Override
	public void show() {
		Gdx.app.log(TAG, "show()");
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		buttons.render();
		
		checkForReady();
	}
	
	@Override
	public void dispose() {
		buttons.dispose();
	}
	
	private void checkForReady() {
		boolean areOtherPlayersReady = true;
		
		for (Player otherPlayer : otherPlayers.values()) {
			if (!otherPlayer.isReady()) areOtherPlayersReady = false;
		}
		
		if (player.isReady() && areOtherPlayersReady) {
			game.setScreen(new GameScreen(game));
		}
	}
	
	private void createPlayer() {
		worldManager.createPlayer(player);
		
		if (!networkManager.isServer()) {
			RequestOtherPlayersPacket request = new RequestOtherPlayersPacket();
			networkManager.send(request);
		}
		
		CreatePlayerPacket packet = new CreatePlayerPacket();
		packet.setName(player.getName());
		packet.setX(player.getBody().getPosition().x);
		packet.setY(player.getBody().getPosition().y);
		packet.setSkinName(player.getSkinName());
		packet.setTeam(player.getTeam());
		networkManager.send(packet);
	}
	
	private void createUI() {
		buttons = new ButtonUI();
		
		buttons.addButton("Join Human", new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
                
				player.setTeam(Team.HUMAN);
				shit();
				System.out.println("sdflskdajfnsdljfdlsfldsflj");
				
				SetTeamPacket packet = new SetTeamPacket();
				packet.setTeam(SetTeamPacket.Team.HUMAN);
				networkManager.send(packet);
				
				player.setSkinName(SkinName.values()[preferences.getInteger("humanSkin")]);
				
				SetSkinNamePacket skinPacket = new SetSkinNamePacket();
				skinPacket.setSkinName(player.getSkinName());
				networkManager.send(skinPacket);
			}
		});
		
		buttons.addButton("Join Zombie", new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
                
				player.setTeam(Team.ZOMBIE);
				shit();
				System.out.println("sdflskdajfnsdljfdlsfldsflj");
				
				SetTeamPacket packet = new SetTeamPacket();
				packet.setTeam(SetTeamPacket.Team.ZOMBIE);
				networkManager.send(packet);
				
				player.setSkinName(SkinName.values()[preferences.getInteger("zombieSkin")]);
				Gdx.app.log(TAG, String.valueOf(player.getSkinName().ordinal()));
				
				SetSkinNamePacket skinPacket = new SetSkinNamePacket();
				skinPacket.setSkinName(player.getSkinName());
				networkManager.send(skinPacket);
			}
		});
		
		String dah = "dah";
		if (networkManager.isServer()) dah = "Start";
		else dah = "Ready";
		
		buttons.addButton(dah, new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
                
                if (networkManager.isServer()) {
                    if (isOtherPlayersReady()) {
                        player.setReady(!player.isReady());
                        shit();
                    }
                } else {
                    player.setReady(!player.isReady());
                    shit();
                }
				
				SetReadyPacket packet = new SetReadyPacket();
				packet.setReady(player.isReady());
				networkManager.send(packet);
			}
		});
        
        buttons.addButton("Leave", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                RemovePlayerPacket packet = new RemovePlayerPacket();
                networkManager.send(packet);
                
                if (networkManager.isServer()) {
                    networkManager.closeServer();
                } else {
                    networkManager.disconnect();
                }
                
                game.setScreen(new MainMenuScreen(game));
            }
        });
		
		buttons.create();
	}
	
	protected boolean isOtherPlayersReady() {
	   boolean areOtherPlayersReady = true;
        
        for (Player otherPlayer : otherPlayers.values()) {
            if (!otherPlayer.isReady()) areOtherPlayersReady = false;
        }
        
        return areOtherPlayersReady;
    }

    private void createLobbyUI() {
	    Stage stage = buttons.getStage();
	    Skin skin = buttons.getSkin();
	    
	    int buttonFontSize = Gdx.graphics.getHeight() / 10;
        BitmapFont font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = font;
        skin.add("default", labelStyle);
        
        labelA = new Label("Server: ", skin);
        labelA.setPosition(50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 500 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        stage.addActor(labelA);
        
        labelB = new Label("Map: ", skin);
        labelB.setPosition(50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 425 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        stage.addActor(labelB);
        
        humansLabel = new Label[6];
        zombiesLabel = new Label[6];
        
        buttonFontSize = Gdx.graphics.getHeight() / 12;
        font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        labelStyle = new LabelStyle();
        labelStyle.font = font;
        skin.add("default", labelStyle);
        
        Label label = new Label("Human", skin);
        label.setPosition(50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 325 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        stage.addActor(label);
        
        label = new Label("Zombie", skin);
        label.setPosition(425 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 325 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        stage.addActor(label);
        
        buttonFontSize = Gdx.graphics.getHeight() / 14;
        font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        labelStyle = new LabelStyle();
        labelStyle.font = font;
        skin.add("default", labelStyle);
        
        for (int i = 0; i < 6; i++) {
            humansLabel[i] = new Label("-", skin);
            humansLabel[i].setPosition(50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, (275 - (50 * i)) * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
            stage.addActor(humansLabel[i]);
        }
        
        for (int i = 0; i < 6; i++) {
            zombiesLabel[i] = new Label("-", skin);
            zombiesLabel[i].setPosition(425 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, (275 - (50 * i)) * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
            stage.addActor(zombiesLabel[i]);
        }
        
        shit();
	}
	
	public void shit() {System.out.println(worldManager.getOtherPlayers().size());
	    List<Player> humans = worldManager.getHumanPlayers();System.out.println("HUMAN SIZE: " + humans.size());
	    for (Player otherPlayer : worldManager.getOtherPlayers().values()) {
	        System.out.println("AAAAAAAA " + otherPlayer.getName() + " " + otherPlayer.getTeam());
	    }
        if (player.getTeam() == Team.HUMAN) humans.add(player);
        
        for (int i = 0; i < humans.size(); i++) {
            humansLabel[i].setText(humans.get(i).getName());
            System.out.println("sdfjids234234234 " + humans.get(i).isReady());
            if (humans.get(i).isReady()) {
                humansLabel[i].setColor(Color.GREEN);
            } else {
                humansLabel[i].setColor(Color.WHITE);
            }
        }
        
        for (int i = humans.size(); i < 6; i++) {
            humansLabel[i].setText("-");
        }
        
        List<Player> zombies = worldManager.getZombiePlayers();System.out.println("ZOMBIE SIZE: " + zombies.size());
        
        if (player.getTeam() == Team.ZOMBIE) zombies.add(player);
        
        for (int i = 0; i < zombies.size(); i++) {
            zombiesLabel[i].setText(zombies.get(i).getName());
            System.out.println("sdfjids234234234 " + zombies.get(i).isReady());
            if (zombies.get(i).isReady()) {
                zombiesLabel[i].setColor(Color.GREEN);
            } else {
                zombiesLabel[i].setColor(Color.WHITE);
            }
        }
        
        for (int i = zombies.size(); i < 6; i++) {
            zombiesLabel[i].setText("-");
        }
        
        if (networkManager.isServer()) {
            labelA.setText("Server: " + player.getName());
        } else {
            if (otherPlayers.size() > 0) labelA.setText("Server: " + ((Player) otherPlayers.values().toArray()[0]).getName());
        }
        
        labelB.setText("Map: " + worldManager.getMap() + " (" + (1 + otherPlayers.size()) + "/" + 6 + ")");
	}
}
