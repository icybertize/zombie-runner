package th.ac.kmitl.it.mdp2014.zombierunner.entities;

import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.SYRINGE_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.GROUND_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.LADDER_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.PLAYER_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.PPM;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.WALL_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.HUMAN_WALL_BITS;
import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.SYRINGE_RADIUS_BITS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetSkinNamePacket.SkinName;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetTeamPacket.Team;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Player extends Sprite {
    
    public static final int SPRITE_WIDTH = 100;
    public static final int SPRITE_HEIGHT = 100;
    
    private static final float ANIMATION_DURATION = 1 / 15f;
    
    public static enum Direction {
        UP, LEFT, RIGHT
    }
    
    private float boxWidth = 17f;
    private float boxHeight = 50;
    
    private BodyDef bodyDef;
    private FixtureDef fixtureDef;
    private FixtureDef infectionFixtureDef;
    
    private int hp;
    private float myFuckingTime;
    private boolean isHPDroping;
    private Direction direction;
    private float speed;
    
    private boolean isReady;
    private boolean isWalkUpDownAble;
    
    private boolean isWalking;
    private boolean isWalkingUpDown;
    private boolean isWalkingLeftRight;
    
    private SkinName skinName;
    private Animation animation;
    private HashMap<String, Animation> animations;
    private float stateTime;
    
    private Team team;
    
    private long skill1StartTime;
    private long skill2StartTime;
    
    private boolean isSkill1Started;
    private boolean isSkill2Started;
    
    private boolean isSkill1Ready;
    private boolean isSkill2Ready;
    
    public Player(String name) {
        super(name);
        
        
        
        bodyDef = new BodyDef();
        bodyDef.fixedRotation = true;
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(ZombieRunner.VIEWPORT_WIDTH / 2, ZombieRunner.VIEWPORT_HEIGHT);
        
        fixtureDef = new FixtureDef();
        
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(boxWidth / PPM, boxHeight / PPM);
        
        fixtureDef.friction = 0;
        fixtureDef.filter.categoryBits = PLAYER_BITS;
        fixtureDef.filter.maskBits = GROUND_BITS | WALL_BITS | LADDER_BITS | HUMAN_WALL_BITS | SYRINGE_BITS | SYRINGE_RADIUS_BITS | WorldManager.INFECTION_BITS;
        fixtureDef.shape = shape;
        
        shape = new PolygonShape();
        shape.setAsBox((boxWidth + 5) / PPM, boxHeight / PPM);
        
        infectionFixtureDef = new FixtureDef();
        infectionFixtureDef.filter.categoryBits = WorldManager.INFECTION_BITS;
        infectionFixtureDef.filter.maskBits = PLAYER_BITS;
        infectionFixtureDef.shape = shape;
        infectionFixtureDef.isSensor = true;
        
        hp = 100;
        isHPDroping = false;
        myFuckingTime = 0;
        direction = Direction.RIGHT;
        speed = 3;
        
        isReady = false;
        isWalkUpDownAble = false;
        
        isWalking = false;
        isWalkingUpDown = false;
        isWalkingLeftRight = false;
        
        stateTime = 0;
        
        List<Team> teams = new ArrayList<Team>();
        Collections.addAll(teams, Team.values());
        Collections.shuffle(teams);
        
        team = teams.get(0);
        Preferences preferences = Gdx.app.getPreferences(ZombieRunner.PREFERENCES_FILE_NAME);

        
        
        switch (team) {
        
        case HUMAN:
            
            skinName = SkinName.values()[preferences.getInteger("humanSkin")];
            
            break;
        
        case ZOMBIE:
            
            skinName = SkinName.values()[preferences.getInteger("zombieSkin")];
            
            break;
        }
    }
    
    public Player(String name, float x, float y, SkinName skinName, Team team) {
        this(name);
        bodyDef.position.set(x, y);
        this.skinName = skinName;
        this.team = team;
    }
    
    public void walkUp() {
        if (isWalkUpDownAble) {
            Gdx.app.log(TAG, "walkUp()");
            
            body.setGravityScale(0);
            body.setLinearVelocity(0, speed);
            
            animation = animations.get("walkUp");
            
            isWalking = true;
            isWalkingUpDown = true;
            
            direction = Direction.UP;
        }
    }
    
    public void walkDown() {
        if (isWalkUpDownAble) {
            Gdx.app.log(TAG, "walkDown()");
            
            body.setGravityScale(0);
            body.setLinearVelocity(0, -speed);
            
            animation = animations.get("walkDown");
            
            isWalking = true;
            isWalkingUpDown = true;
        }
    }
    
    public void walkLeft() {
        Gdx.app.log(TAG, "walkLeft()");
        
        body.setGravityScale(1);
        body.setLinearVelocity(-speed, 0);
        
        animation = animations.get("walkLeft");
        
        isWalking = true;
        isWalkingLeftRight = true;
        
        direction = Direction.LEFT;
    }
    
    public void walkRight() {
        Gdx.app.log(TAG, "walkRight()");
        
        body.setGravityScale(1);
        body.setLinearVelocity(speed, 0);
        
        animation = animations.get("walkRight");
        
        isWalking = true;
        isWalkingLeftRight = true;
        
        direction = Direction.RIGHT;
    }
    
    public void stopWalking() {
        Gdx.app.log(TAG, "stopWalking()");
        
        body.setGravityScale(1);
        body.setLinearVelocity(0, 0);
        
        isWalking = false;
        isWalkingUpDown = false;
        isWalkingLeftRight = false;
    }
    
    public void doneSkill1() {
        switch (team) {
        case HUMAN:
            Gdx.app.log(TAG, "doneSkill1()");
            isSkill1Started = false;
            fixtureDef.filter.maskBits = GROUND_BITS | WALL_BITS | LADDER_BITS | SYRINGE_BITS | HUMAN_WALL_BITS;
            body.getFixtureList().get(0).setFilterData(fixtureDef.filter);
            
            break;
        
        case ZOMBIE:
            Gdx.app.log(TAG, "doneSkill1()");
            isSkill1Started = false;
            Player.this.speed = 3;
            
            break;
        }
        
    }
    
    public void doneSkill2() {
        switch (team) {
        case HUMAN:
            Gdx.app.log(TAG, "doneSkill2()");
            isSkill2Started = false;
            
            break;
        
        case ZOMBIE:
            Gdx.app.log(TAG, "doneSkill2()");
            isSkill2Started = false;
            
            break;
        
        }
    }
    
    public void doSkill1() {
    	if(isSkill1Ready){
    		switch (team) {
            
            case HUMAN:
                
                Gdx.app.log(TAG, "doSkill1()");
                isSkill1Started = true;
                skill1StartTime = System.currentTimeMillis();
                fixtureDef.filter.maskBits = WALL_BITS | LADDER_BITS | SYRINGE_BITS | HUMAN_WALL_BITS;
                body.getFixtureList().get(0).setFilterData(fixtureDef.filter);
                
                break;
            
            case ZOMBIE:
                
                Gdx.app.log(TAG, "doSkill1()");
                isSkill1Started = true;
                skill1StartTime = System.currentTimeMillis();
                Player.this.speed = 5;
                
                break;
            }
    	}
        
    }
    
    public void doSkill2() {
    	if(isSkill2Ready){
    		switch (team) {
            
            case HUMAN:
                
                Gdx.app.log(TAG, "doSkill2()");
                isSkill2Started = true;
                skill2StartTime = System.currentTimeMillis();
                
                break;
            
            case ZOMBIE:
                
                Gdx.app.log(TAG, "doSkill2()");
                isSkill2Started = true;
                skill2StartTime = System.currentTimeMillis();
                
                break;
            }
    	}
        
    }
    
    public void createAnimations() {
        animations = new HashMap<String, Animation>();
        
        switch (skinName) {
        
        case HUMAN:
            
            createAnimation("walkUp", "characters/human/walkUp.png", 1 / 10f);
            createAnimation("walkDown", "characters/human/walkUp.png", 1 / 10f);
            createAnimation("walkLeft", "characters/human/walkLeft.png");
            createAnimation("walkRight", "characters/human/walkRight.png");
            
            break;
        
        case BATMAN:
            
            createAnimation("walkUp", "characters/batman/walkUp.png", 1 / 10f);
            createAnimation("walkDown", "characters/batman/walkUp.png", 1 / 10f);
            createAnimation("walkLeft", "characters/batman/walkLeft.png");
            createAnimation("walkRight", "characters/batman/walkRight.png");
            
            break;
        
        case ZOMBIE:
            
            createAnimation("walkUp", "characters/zombie/walkUp.png", 1 / 10f);
            createAnimation("walkDown", "characters/zombie/walkUp.png", 1 / 10f);
            createAnimation("walkLeft", "characters/zombie/walkLeft.png");
            createAnimation("walkRight", "characters/zombie/walkRight.png");
            
            break;
        
        case ULTRON:
            
            createAnimation("walkUp", "characters/ultron/walkUp.png", 1 / 10f);
            createAnimation("walkDown", "characters/ultron/walkUp.png", 1 / 10f);
            createAnimation("walkLeft", "characters/ultron/walkLeft.png");
            createAnimation("walkRight", "characters/ultron/walkRight.png");
            
            break;
        }
        
        animation = animations.get("walkRight");
    }
    
    public boolean isCreatedAnimations() {
        return animations != null;
    }
    
    public void render(Batch batch) {
        stateTime += Gdx.graphics.getDeltaTime();
        
        if (isHPDroping) {
            if (myFuckingTime < 1) myFuckingTime += Gdx.graphics.getDeltaTime();
            else {
                hp-=5;
                myFuckingTime = 0;
            }
        } else {
            myFuckingTime = 0;
        }
        
        batch.begin();
        
        if (isWalking) {
            batch.draw(animation.getKeyFrame(stateTime, true), body.getPosition().x * PPM - SPRITE_WIDTH / 2, body.getPosition().y * PPM - SPRITE_HEIGHT / 2);
        } else {
            batch.draw(animation.getKeyFrames()[0], body.getPosition().x * PPM - SPRITE_WIDTH / 2, body.getPosition().y * PPM - SPRITE_HEIGHT / 2);
        }
        
        batch.end();
    }
    
    public BodyDef getBodyDef() {
        return bodyDef;
    }
    
    public FixtureDef getFixtureDef() {
        return fixtureDef;
    }
    
    
    public FixtureDef getInfectionFixtureDef() {
        return infectionFixtureDef;
    }

    public int getHP() {
        return hp;
    }
    
    public void setHP(int hp) {
        this.hp = hp;
    }
    
    
    public boolean isHPDroping() {
        return isHPDroping;
    }

    
    public void setHPDroping(boolean isHPDroping) {
        this.isHPDroping = isHPDroping;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
    
    public void setReady(boolean isReady) {
        this.isReady = isReady;
    }
    
    public boolean isReady() {
        return isReady;
    }
    
    public boolean isWalkUpDownAble() {
        return isWalkUpDownAble;
    }
    
    public boolean isWalking() {
        return isWalking;
    }
    
    public boolean isWalkingLeftRight() {
        return isWalkingLeftRight;
    }
    
    public void setWalkUpDownAble(boolean isWalkUpDownAble) {
        this.isWalkUpDownAble = isWalkUpDownAble;
    }
    
    public boolean isWalkingUp() {
        return isWalkingUpDown;
    }
    
    public void setWalkingUp(boolean isWalkingUp) {
        this.isWalkingUpDown = isWalkingUp;
    }
    
    public Team getTeam() {
        return team;
    }
    
    public void setTeam(Team team) {
        this.team = team;
    }
    
    private void createAnimation(String key, String path) {
        createAnimation(key, path, ANIMATION_DURATION);
    }
    
    private void createAnimation(String key, String path, float duration) {
        Texture walkLeftTexture = new Texture(path);
        TextureRegion[][] walkLeftAllFrames = TextureRegion.split(walkLeftTexture, SPRITE_WIDTH, SPRITE_HEIGHT);
        TextureRegion[] walkLeftFrames = walkLeftAllFrames[0];
        Animation walkLeftAnimation = new Animation(duration, walkLeftFrames);
        
        animations.put(key, walkLeftAnimation);
    }
    
    public SkinName getSkinName() {
        return skinName;
    }
    
    public void setSkinName(SkinName skinName) {
        this.skinName = skinName;
    }
    
    public Color getTeamColor() {
        switch (team) {
        
        case HUMAN:
            
            return Color.BLUE;
            
        case ZOMBIE:
            
            return Color.RED;
            
        default:
            
            return Color.BLACK;
        }
    }
    
    public long getSkill1StartTime() {
        return skill1StartTime;
    }
    
    public Direction getDirection() {
        return direction;
    }
    
    public void setDirection(Direction direction) {
        this.direction = direction;
    }
    
    public boolean isSkill1Started() {
        return isSkill1Started;
    }
    
    public boolean isSkill2Started() {
        return isSkill2Started;
    }

	public boolean isSkill1Ready() {
		return isSkill1Ready;
	}

	public void setSkill1Ready(boolean isSkill1Ready) {
		this.isSkill1Ready = isSkill1Ready;
	}

	public boolean isSkill2Ready() {
		return isSkill2Ready;
	}

	public void setSkill2Ready(boolean isSkill2Ready) {
		this.isSkill2Ready = isSkill2Ready;
	}

	public long getSkill2StartTime() {
		return skill2StartTime;
	}
    
    
}
