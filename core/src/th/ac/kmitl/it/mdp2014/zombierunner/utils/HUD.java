package th.ac.kmitl.it.mdp2014.zombierunner.utils;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Player;
import th.ac.kmitl.it.mdp2014.zombierunner.generators.FontGenerator;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.SetPositionPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.DoSkill1Packet;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.DoSkill2Packet;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.StopWalkingPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkLeftPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkRightPacket;
import th.ac.kmitl.it.mdp2014.zombierunner.network.packets.player.methods.WalkUpPacket;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class HUD {
	
	private ZombieRunner game;
	
	private WorldManager worldManager;
	private NetworkManager networkManager;
	
	private Player player;
	
	private OrthographicCamera hudCamera;
	
	private Stage stage;
	
	private SpriteBatch batch;
	
	private Texture healthIcon;
	private Texture syringeIcon;
	
	private BitmapFont hpFont;
	private BitmapFont modeFont;
	
	private Image wIcon;
	private Image aIcon;
	private Image sIcon;
	private Image dIcon;
	
	private Image skill1Icon;
	private Image skill2Icon;
	
	private ImageButton observerLeftButton;
    private ImageButton observerRightButton;
    
    private ImageButton skill1Button;
    private ImageButton skill2Button;
    
    private boolean isSkill1Ready;
    private boolean isSkill2Ready;

	
    public boolean isSkill1Ready() {
        return isSkill1Ready;
    }

    
    public void setSkill1Ready(boolean isSkill1Ready) {
        if (isSkill1Ready != this.isSkill1Ready) {
            System.out.println("new" + isSkill1Ready);
            System.out.println("old" + this.isSkill1Ready);
            if (isSkill1Ready) {
                switch (player.getTeam()) {
                
                case HUMAN:
                    
                    skill1Icon = new Image(new Texture("hud/humanSkill1.png"));
                    skill2Icon = new Image(new Texture("hud/humanSkill2.png"));
                    
                    break;
                    
                case ZOMBIE:
                    
                    skill1Icon = new Image(new Texture("hud/zombieSkill1.png"));
                    skill2Icon = new Image(new Texture("hud/zombieSkill2.png"));
                    
                    break;
                }
            } else {
                switch (player.getTeam()) {
                
                case HUMAN:
                    
                    skill1Icon = new Image(new Texture("hud/humanSkill1Cooldown.png"));
                    skill2Icon = new Image(new Texture("hud/humanSkill2Cooldown.png"));
                    
                    break;
                    
                case ZOMBIE:
                    
                    skill1Icon = new Image(new Texture("hud/zombieSkill1Cooldown.png"));
                    skill2Icon = new Image(new Texture("hud/zombieSkill2Cooldown.png"));
                    
                    break;
                }
            }
            ImageButtonStyle style = new ImageButtonStyle();
            style.up = skill1Icon.getDrawable();
            skill1Button.setStyle(style);
        }
        this.isSkill1Ready = isSkill1Ready;
    }

    
    public boolean isSkill2Ready() {
        return isSkill2Ready;
    }

    
    public void setSkill2Ready(boolean isSkill2Ready) {
        if (isSkill2Ready != this.isSkill2Ready) {
            System.out.println("new" + isSkill2Ready);
            System.out.println("old" + this.isSkill2Ready);
            if (isSkill2Ready) {
                switch (player.getTeam()) {
                
                case HUMAN:
                    
                    skill1Icon = new Image(new Texture("hud/humanSkill1.png"));
                    skill2Icon = new Image(new Texture("hud/humanSkill2.png"));
                    
                    break;
                    
                case ZOMBIE:
                    
                    skill1Icon = new Image(new Texture("hud/zombieSkill1.png"));
                    skill2Icon = new Image(new Texture("hud/zombieSkill2.png"));
                    
                    break;
                }
            } else {
                switch (player.getTeam()) {
                
                case HUMAN:
                    
                    skill1Icon = new Image(new Texture("hud/humanSkill1Cooldown.png"));
                    skill2Icon = new Image(new Texture("hud/humanSkill2Cooldown.png"));
                    
                    break;
                    
                case ZOMBIE:
                    
                    skill1Icon = new Image(new Texture("hud/zombieSkill1Cooldown.png"));
                    skill2Icon = new Image(new Texture("hud/zombieSkill2Cooldown.png"));
                    
                    break;
                }
            }
            ImageButtonStyle style = new ImageButtonStyle();
            style.up = skill2Icon.getDrawable();
            skill2Button.setStyle(style);
        }
        this.isSkill2Ready = isSkill2Ready;
    }

    public HUD(ZombieRunner game) {
		this.game = game;
		
		worldManager = game.getWorldManager();
		networkManager = game.getNetworkManager();
		
		player = game.getWorldManager().getPlayer();
		
		batch = new SpriteBatch();
		
		hudCamera = new OrthographicCamera();
		hudCamera.setToOrtho(false, ZombieRunner.SCREEN_WIDTH, ZombieRunner.SCREEN_HEIGHT);
		hudCamera.update();
		
		stage = new Stage();
		stage.getViewport().setCamera(hudCamera);
		
		healthIcon = new Texture("hud/health.png");
		syringeIcon = new Texture("hud/syringe.png");
		
		hpFont = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Italic.ttf", 50);
		hpFont.setColor(Color.RED);
		
		modeFont = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Italic.ttf", 50);
		
		wIcon = new Image(new Texture("hud/w.png"));
		aIcon = new Image(new Texture("hud/a.png"));
		sIcon = new Image(new Texture("hud/s.png"));
		dIcon = new Image(new Texture("hud/d.png"));
		
		switch (player.getTeam()) {
		
		case HUMAN:
			
			skill1Icon = new Image(new Texture("hud/humanSkill1.png"));
			skill2Icon = new Image(new Texture("hud/humanSkill2.png"));
			
			break;
			
		case ZOMBIE:
			
			skill1Icon = new Image(new Texture("hud/zombieSkill1.png"));
			skill2Icon = new Image(new Texture("hud/zombieSkill2.png"));
			
			break;
		}
		
		observerLeftButton = new ImageButton(new Image(new Texture("buttons/arrowLeft.png")).getDrawable());
		observerLeftButton.setSize(50, 50);
		observerLeftButton.setPosition(325, ZombieRunner.SCREEN_HEIGHT - 50 - 22);
		observerLeftButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
               worldManager.cameraPlayerIndex--;
               if (worldManager.cameraPlayerIndex < 0) worldManager.cameraPlayerIndex = worldManager.getOtherPlayers().size() - 1;
            }
		});
		observerLeftButton.setVisible(false);
		stage.addActor(observerLeftButton);
		
        observerRightButton = new ImageButton(new Image(new Texture("buttons/arrowRight.png")).getDrawable());
        observerRightButton.setSize(50, 50);
        observerRightButton.setPosition(655, ZombieRunner.SCREEN_HEIGHT - 50 -  22);
        observerRightButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
               worldManager.cameraPlayerIndex++;
               if (worldManager.cameraPlayerIndex > worldManager.getOtherPlayers().size() - 1) worldManager.cameraPlayerIndex = 0;
            }
        });
        observerRightButton.setVisible(false);
        stage.addActor(observerRightButton);
		
		ImageButton wButton = new ImageButton(wIcon.getDrawable());
		wButton.setSize(100, 100);
		wButton.setPosition(aIcon.getWidth(), 20 + sIcon.getWidth() / 2);
		
		wButton.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				player.walkUp();
				
				WalkUpPacket packet = new WalkUpPacket();
				networkManager.send(packet);
				
				return true;
			}
			
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				player.stopWalking();
				
				StopWalkingPacket packet = new StopWalkingPacket();
				networkManager.send(packet);
				
				SetPositionPacket setPacket = new SetPositionPacket();
				setPacket.setX(player.getBody().getPosition().x);
				setPacket.setY(player.getBody().getPosition().y);
				networkManager.send(setPacket);
			}
		});
		
		stage.addActor(wButton);
		
		ImageButton aButton = new ImageButton(aIcon.getDrawable());
		aButton.setSize(100, 100);
		aButton.setPosition(20, 20);
		
		aButton.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				player.walkLeft();
				
				WalkLeftPacket packet = new WalkLeftPacket();
				networkManager.send(packet);
				
				return true;
			}
			
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				player.stopWalking();
				
				StopWalkingPacket packet = new StopWalkingPacket();
				networkManager.send(packet);
				
				SetPositionPacket setPacket = new SetPositionPacket();
				setPacket.setX(player.getBody().getPosition().x);
				setPacket.setY(player.getBody().getPosition().y);
				networkManager.send(setPacket);
			}
		});
		
		stage.addActor(aButton);
		
//		ImageButton sButton = new ImageButton(sIcon.getDrawable());
//		sButton.setSize(100, 100);
//		sButton.setPosition(sIcon.getWidth(), 20);
//		
//		sButton.addListener(new InputListener() {
//			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//				player.walkDown();
//				
//				WalkDownPacket packet = new WalkDownPacket();
//				networkManager.send(packet);
//				
//				return true;
//			}
//			
//			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
//				player.stopWalking();
//				
//				StopWalkingPacket packet = new StopWalkingPacket();
//				networkManager.send(packet);
//				
//				SetPositionPacket setPacket = new SetPositionPacket();
//				setPacket.setX(player.getBody().getPosition().x);
//				setPacket.setY(player.getBody().getPosition().y);
//				networkManager.send(setPacket);
//			}
//		});
//		
//		stage.addActor(sButton);
		
		ImageButton dButton = new ImageButton(dIcon.getDrawable());
		dButton.setSize(100, 100);
		dButton.setPosition(aIcon.getWidth() * 2 - 20, 20);
		
		dButton.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				player.walkRight();
				
				WalkRightPacket packet = new WalkRightPacket();
				networkManager.send(packet);
				
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				player.stopWalking();
				
				StopWalkingPacket packet = new StopWalkingPacket();
				networkManager.send(packet);
				
				SetPositionPacket setPacket = new SetPositionPacket();
				setPacket.setX(player.getBody().getPosition().x);
				setPacket.setY(player.getBody().getPosition().y);
				networkManager.send(setPacket);
			}
		});
		
		stage.addActor(dButton);
		
		skill1Button = new ImageButton(skill1Icon.getDrawable());
		skill1Button.setPosition(ZombieRunner.SCREEN_WIDTH - skill1Icon.getWidth() - skill2Icon.getWidth() - 20 - 20, 20);
		
		skill1Button.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
			    switch (player.getTeam()) {
			    
                case HUMAN:
                    
                    HUD.this.game.getSounds().play(HUD.this.game.getSounds().digDownSound);
                    break;
                    
                case ZOMBIE:

                    HUD.this.game.getSounds().play(HUD.this.game.getSounds().doSprintSound);
                    break;
			    }
			    
				player.doSkill1();
				
				DoSkill1Packet packet = new DoSkill1Packet();
				networkManager.send(packet);
			}
		});
		
		stage.addActor(skill1Button);
		
		skill2Button = new ImageButton(skill2Icon.getDrawable());
		skill2Button.setPosition(ZombieRunner.SCREEN_WIDTH - skill2Icon.getWidth() - 20, 20);
		
		skill2Button.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
			    switch (player.getTeam()) {
                
                case HUMAN:

                    HUD.this.game.getSounds().play(HUD.this.game.getSounds().buildWallSound);
                    break;
                    
                case ZOMBIE:

                    HUD.this.game.getSounds().play(HUD.this.game.getSounds().blastWallSound);
                    break;
                }
                
				player.doSkill2();
                
                DoSkill2Packet packet = new DoSkill2Packet();
                networkManager.send(packet);
			}
		});
		
		stage.addActor(skill2Button);
	}

	public void render() {
		batch.setProjectionMatrix(hudCamera.combined);
		
		batch.begin();
		
		batch.draw(healthIcon, 20, ZombieRunner.SCREEN_HEIGHT - healthIcon.getHeight() - 20);
		batch.draw(syringeIcon, ZombieRunner.SCREEN_WIDTH - syringeIcon.getWidth() - 20, ZombieRunner.SCREEN_HEIGHT - healthIcon.getHeight() - 20);
		
		hpFont.draw(batch, String.valueOf(player.getHP()), healthIcon.getWidth() + 20 + 10, ZombieRunner.SCREEN_HEIGHT - 27);
        hpFont.draw(batch, String.valueOf(worldManager.getCollectedSyringes()) + "/" + String.valueOf(worldManager.getSyringes().size()), ZombieRunner.SCREEN_WIDTH - syringeIcon.getWidth() - 20 - 27 - 27 - 20, ZombieRunner.SCREEN_HEIGHT - 27);
		
		if (player.getHP() <= 0) {
		    observerLeftButton.setVisible(true);
            observerRightButton.setVisible(true);
		    modeFont.draw(batch, "Observer Mode", 380, ZombieRunner.SCREEN_HEIGHT - 27);
		}
        
        batch.end();
		
		stage.act();
		stage.draw();
	}

	public void dispose() {
		stage.dispose();
		
		healthIcon.dispose();
		syringeIcon.dispose();
	}

	public InputProcessor getStage() {
		return stage;
	}

}
