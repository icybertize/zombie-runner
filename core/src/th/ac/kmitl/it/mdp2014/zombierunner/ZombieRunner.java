package th.ac.kmitl.it.mdp2014.zombierunner;

import static th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager.PPM;
import th.ac.kmitl.it.mdp2014.zombierunner.entities.Player;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.screens.MainMenuScreen;
import th.ac.kmitl.it.mdp2014.zombierunner.screens.SettingsScreen;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.Sounds;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class ZombieRunner extends Game {
	
	public static final String PREFERENCES_FILE_NAME = "zombierunner.xml";
	
	public static final int SCREEN_WIDTH = 1024;
	public static final int SCREEN_HEIGHT = 600;
	
	public static final float VIEWPORT_WIDTH = SCREEN_WIDTH / PPM;
	public static final float VIEWPORT_HEIGHT = SCREEN_HEIGHT / PPM;
	
	private WorldManager worldManager;
	private NetworkManager networkManager;
	
	private Sounds sounds;
	
	@Override
	public void create() {
		worldManager = new WorldManager(this);
		networkManager = new NetworkManager(this);
		
        sounds = new Sounds();
		
		Preferences preferences = Gdx.app.getPreferences(PREFERENCES_FILE_NAME);
		
		if (preferences.contains("playerName") &&
			preferences.contains("humanSkin") &&
			preferences.contains("zombieSkin") &&
			preferences.contains("isPlaySound")
		) {
		    sounds.setPlaySound(preferences.getBoolean("isPlaySound"));
            sounds.loop(sounds.menuMusic);
			initialize(preferences.getString("playerName"));
		} else {
            sounds.loop(sounds.menuMusic);
			setScreen(new SettingsScreen(this));
		}
	}

    @Override
    public void dispose() {
        sounds.dispose();
    }

    public void initialize(String playerName) {
        worldManager.createWorld();

        Player player = new Player(playerName);
        worldManager.setPlayer(player);

        networkManager.startClient();

        setScreen(new MainMenuScreen(this));
    }
	
	public WorldManager getWorldManager() {
		return worldManager;
	}
	
	public NetworkManager getNetworkManager() {
		return networkManager;
	}
    
    
    public void setWorldManager(WorldManager worldManager) {
        this.worldManager = worldManager;
    }

    
    public void setNetworkManager(NetworkManager networkManager) {
        this.networkManager = networkManager;
    }

    public Sounds getSounds() {
        return sounds;
    }
	
}
