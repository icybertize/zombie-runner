package th.ac.kmitl.it.mdp2014.zombierunner.entities;

import com.badlogic.gdx.physics.box2d.Body;

public abstract class Sprite {
	
	protected String name;
	protected Body body;
    
    protected String TAG;
	
	protected Sprite(String name) {
		this.name = name;
		TAG = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Body getBody() {
		return body;
	}
	
	public void setBody(Body body) {
		this.body = body;
	}

}
