package th.ac.kmitl.it.mdp2014.zombierunner.screens;

import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;
import th.ac.kmitl.it.mdp2014.zombierunner.generators.FontGenerator;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.NetworkManager;
import th.ac.kmitl.it.mdp2014.zombierunner.managers.WorldManager;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.ButtonUI;
import th.ac.kmitl.it.mdp2014.zombierunner.utils.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class CreateGameScreen extends ScreenAdapter {
	
	private static final String TAG = "CreateGameScreen";
	
	private ZombieRunner game;
	
	private WorldManager worldManager;
	private NetworkManager networkManager;
	
	private ButtonUI buttons;
	
	private SpriteBatch batch;
	
	private Map map;
	private Texture mapTexture;
	private TiledMap tiledMap;
	private Label labelA;
    private Label labelB;
	
	public CreateGameScreen(ZombieRunner game) {
		this.game = game;
		
		worldManager = game.getWorldManager();
		networkManager = game.getNetworkManager();
		
		batch = new SpriteBatch();
		
		map = Map.BASIC;
		mapTexture = new Texture("maps/preview/" + map + ".png");
		tiledMap = new TmxMapLoader().load("maps/" + map + ".tmx");
		
		createUI();
		
		createCreateGameUI();
	}
	
	@Override
	public void show() {
		Gdx.app.log(TAG, "show()");
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		buttons.render();
		
		batch.begin();
		batch.draw(mapTexture, 175 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 350 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 350 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
		batch.end();
	}
	
	private void createUI() {
		buttons = new ButtonUI();
		
		buttons.addButton("OK", new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
                worldManager.setMap(map);
				try {
					networkManager.startServer("Test Server");
					game.setScreen(new LobbyScreen(game));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		buttons.addButton("Cancel", new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
                game.getSounds().play(game.getSounds().clickSound);
				game.setScreen(new MainMenuScreen(game));
			}
		});
		
		buttons.create();
	}
	
	private void createCreateGameUI() {
        Stage stage = buttons.getStage();
        Skin skin = buttons.getSkin();
        
        int buttonFontSize = Gdx.graphics.getHeight() / 10;
        BitmapFont font = FontGenerator.generateFont("fonts/RobotoCondensed/RobotoCondensed-Regular.ttf", buttonFontSize);
        
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = font;
        skin.add("default", labelStyle);
        
        labelA = new Label("Max Player: " + (CharSequence) tiledMap.getProperties().get("maxPlayer"), skin);
        labelA.setPosition(50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 500 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        stage.addActor(labelA);
        
        labelB = new Label("Syringes: " + (CharSequence) tiledMap.getProperties().get("syringes"), skin);
        labelB.setPosition(50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 425 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        stage.addActor(labelB);
        
        ImageButton leftButton = new ImageButton(new Image(new Texture("buttons/arrowLeft.png")).getDrawable());
        leftButton.setPosition(50 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 200 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        leftButton.setSize(100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        leftButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                int i = map.ordinal();
                if (i == 0) i = Map.values().length - 1;
                else i--;
                map = Map.values()[i];
                mapTexture = new Texture("maps/preview/" + map + ".png");
                tiledMap = new TmxMapLoader().load("maps/" + map + ".tmx");
                labelA.setText("Max Player: " + (CharSequence) tiledMap.getProperties().get("maxPlayer"));
                labelB.setText("Syringes: " + (CharSequence) tiledMap.getProperties().get("syringes"));
            }
        });
        stage.addActor(leftButton);
        
        ImageButton rightButton = new ImageButton(new Image(new Texture("buttons/arrowRight.png")).getDrawable());
        rightButton.setPosition(550 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 200 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        rightButton.setSize(100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH, 100 * Gdx.graphics.getWidth() / ZombieRunner.SCREEN_WIDTH);
        rightButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                int i = map.ordinal();
                if (i == Map.values().length - 1) i = 0;
                else i++;
                map = Map.values()[i];
                mapTexture = new Texture("maps/preview/" + map + ".png");
                tiledMap = new TmxMapLoader().load("maps/" + map + ".tmx");
                tiledMap = new TmxMapLoader().load("maps/" + map + ".tmx");
                labelA.setText("Max Player: " + (CharSequence) tiledMap.getProperties().get("maxPlayer"));
                labelB.setText("Syringes: " + (CharSequence) tiledMap.getProperties().get("syringes"));
            }
        });
        stage.addActor(rightButton);
	}

}
