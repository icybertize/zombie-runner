package th.ac.kmitl.it.mdp2014.zombierunner.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import th.ac.kmitl.it.mdp2014.zombierunner.ZombieRunner;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.width = ZombieRunner.SCREEN_WIDTH;
		config.height = ZombieRunner.SCREEN_HEIGHT;
		
		new LwjglApplication(new ZombieRunner(), config);
	}
}
